void connectToWifi() {
  WiFi.begin(wifiSsid.c_str(), wifiPassword.c_str());

  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
  }
  Serial.print("Wifi client IP: ");
  Serial.println(WiFi.localIP());
}

void scanWiFi() {
  int n = WiFi.scanNetworks();
  Serial.print("Wifi Scan count: ");
  Serial.println(n);

  if (n != 0) {
    for (int i = 0; i < n; ++i) {
      String ssid = WiFi.SSID(i);
      int rssi = WiFi.RSSI(i);
      wifiRes.insert(std::make_pair(ssid, rssi));
    }
  }
}

void printWifiRes() {
  Serial.println("WifiRes: ");
  for (auto element : wifiRes) {
    Serial.print(element.first.c_str());
    Serial.print(": ");
    Serial.print(element.second);
    Serial.println();
  }
}
