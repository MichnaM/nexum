#include <WiFi.h>
#include <WiFiClient.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>
#include <base64.h>

///// config /////

String dmID = "dm-1";
String token = "nexum";

String nexumBaseURL = "https://michnam.pl/api"; 

String wifiSsid = "intanetto";
String wifiPassword = "polska123";


///// tweak /////

int bleScanTime = 5; // in seconds


///// private /////
StaticJsonDocument<10240> configs;
StaticJsonDocument<2048> control;
String configHash = "";

std::map<String, int> wifiRes;
std::map<String, int> bleRes;


BLEScan* bleScanner;


void setup() {
  Serial.begin(115200);
  Serial.println("\nWelcome to IoT Nexum - DeviceManager");
  connectToWifi();
  scanBleSetup();
  updateConfigs();
  updateControl();
  delay(500);
}

void loop() {
  uint64_t startTime = millis(); 

  // scan nearby wifi and ble devices
  scanWiFi();
  scanBle();

  // send metrics requests and send data to nexum
  handleDevices();

  // send commands from nexum to devices
  handleControl();

//  printWifiRes();
//  printBleRes(); 
  
  // clean memory after run, and wait till next one
  clearRes();
  int processingTime = millis() - startTime;
  double interval = configs["interval"] ;
  int timeLeft = interval * 1000 - processingTime;
  Serial.print("\nWaiting: ");
  Serial.println(timeLeft);
  if (timeLeft > 0)
    delay(timeLeft);

  // update control only after waiting to have up to date data
  updateControl();
}

void handleDevices() {
  JsonArray deviceConfigs = configs["devices"].as<JsonArray>();
  for (JsonObject device : deviceConfigs) {
    DynamicJsonDocument json(1024);
    String result;
    String deviceID = device["deviceID"];
    String metricUrl = device["config"]["url"];
    
    bool wifiEnabled = device["config"]["wifiMetric"]["enabled"];
    String wifiMetricName = device["config"]["wifiMetric"]["name"];
    String wifiSSID = device["config"]["wifiMetric"]["ssid"];
    
    bool bleEnabled = device["config"]["btMetric"]["enabled"];
    String bleMetricName = device["config"]["btMetric"]["name"];
    String bleName = device["config"]["btMetric"]["btName"]; 
    
    bool latencyEnabled = device["config"]["latencyMetric"]["enabled"];
    String latencyMetricName = device["config"]["latencyMetric"]["name"];
    String latencyUrl = device["config"]["latencyMetric"]["url"];


    // create result
    json[0]["deviceID"] = deviceID;
    
    // calculate WiFi
    json[0]["wifiData"]["name"] = wifiMetricName;
    auto wifiRSSI = wifiRes.find(wifiSSID);
    if (wifiRSSI != wifiRes.end())
      json[0]["wifiData"]["value"] = wifiRSSI->second;

    // calculate BLE
    json[0]["btData"]["name"] = bleMetricName;
    auto bleRSSI = bleRes.find(bleName);
    if (bleRSSI != bleRes.end())
      json[0]["btData"]["value"] = bleRSSI->second;

    // calculate Latency
    int latency = -1;
    if (latencyEnabled)
      latency = checkLatency(latencyUrl);
    json[0]["latencyData"]["name"] = bleMetricName;
    if (latency != -1)
      json[0]["latencyData"]["value"] = latency;

    // retreive custom metrics json
    json[0].createNestedObject("metricsRaw");
    if (metricUrl != "") {
      HTTPClient http;
      http.begin(metricUrl);
      int httpResponseCode = http.GET();
      Serial.print("HTTP Response code - ");
      Serial.print(metricUrl);
      Serial.print(" : ");
      Serial.println(httpResponseCode);
      String metricRawJson = http.getString();
      json[0]["metricsRaw"] = serialized(metricRawJson);
      http.end();
    }
  
    // send to nexum
    serializeJson(json, result);
    Serial.print("Sending to nexum: ");
    Serial.println(result);
    String auth = base64::encode(dmID + ":" + token);
    HTTPClient httpNexum;
    String nexumDataUrl = nexumBaseURL + "/dm/data";
    httpNexum.begin(nexumDataUrl);
    httpNexum.addHeader("Authorization", "Basic " + auth);
    httpNexum.addHeader("Content-Type", "application/json");
    int httpNexumResponseCode = httpNexum.POST(result);
    Serial.print("HTTP Response code - ");
    Serial.print(nexumDataUrl);
    Serial.print(" : ");
    Serial.println(httpNexumResponseCode);
    httpNexum.end();
  }
}


void clearRes() {
  wifiRes.clear();
  bleRes.clear();
}
