void updateConfigs() {
  String payload = sendNexumGetRequest(nexumBaseURL + "/dm/config");
  DeserializationError error = deserializeJson(configs, payload);
}

void updateControl() {
  String payload = sendNexumGetRequest(nexumBaseURL + "/dm/control");
  DeserializationError error = deserializeJson(control, payload);
}

void handleControl() {
  // update config if necessary
  String newConfigHash = control["configHash"];
  Serial.print("Got configHash: ");
  Serial.println(newConfigHash);
  if (configHash != "" && configHash != newConfigHash)
    updateConfigs();
  configHash = newConfigHash;
    

  // handle commands
  JsonArray commands = control["commands"].as<JsonArray>();
  for (JsonObject cmd : commands) {
    String url = cmd["url"];
    String body = cmd["body"];
    if (url != "") {
      HTTPClient http;
    http.begin(url);
    http.addHeader("Content-Type", "application/json");
    int httpResponseCode = http.POST(body);
    Serial.print("HTTP Response code ");
    Serial.print(url);
    Serial.print(": ");
    Serial.println(httpResponseCode);
    http.end();
    }
  }
}

String sendNexumGetRequest(String url) {
  String auth = base64::encode(dmID + ":" + token);
  HTTPClient http;
  http.begin(url);
  http.addHeader("Authorization", "Basic " + auth);
  int httpResponseCode = http.GET();
  Serial.print("HTTP Response code ");
  Serial.print(url);
  Serial.print(": ");
  Serial.println(httpResponseCode);
  String payload = http.getString();
  http.end();
  return payload;
}

int checkLatency(String url) {
  HTTPClient http;
  http.begin(url);
  uint64_t startTime = millis(); 
  int httpResponseCode = http.GET();
  int latency = millis() - startTime;
  http.end();
  return latency;
}
