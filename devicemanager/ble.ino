class BleScanResult: public BLEAdvertisedDeviceCallbacks {
    void onResult(BLEAdvertisedDevice advertisedDevice) {
      String bleName = advertisedDevice.getName().c_str();
      String addr = advertisedDevice.getAddress().toString().c_str();
      int rssi = advertisedDevice.getRSSI();
      bleRes.insert(std::make_pair(bleName, rssi));
      bleRes.insert(std::make_pair(addr, rssi));
    }
};

void scanBleSetup() {
  BLEDevice::init("");
  bleScanner = BLEDevice::getScan(); //create new scan
  bleScanner -> setAdvertisedDeviceCallbacks(new BleScanResult());
  bleScanner -> setActiveScan(true); //active scan uses more power, but get results faster
  bleScanner -> setInterval(100);
  bleScanner -> setWindow(99);  // less or equal setInterval value
  Serial.println("Ble scan setup finished\n.");
}

void scanBle() {
  BLEScanResults foundDevices = bleScanner->start(bleScanTime, false);
  Serial.print("BLE scan count: ");
  Serial.println(foundDevices.getCount());
  bleScanner->clearResults();
}

void printBleRes() {
  Serial.println("Ble res: ");
  for (auto element : bleRes) {
    Serial.print(element.first.c_str());
    Serial.print(": ");
    Serial.print(element.second);
    Serial.println();
  }
}
