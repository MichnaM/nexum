#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEServer.h>
#include <BLEAdvertisedDevice.h>
#include <ArduinoJson.h>

String wifiSsid = "intanetto";
String wifiPassword = "polska123";

WebServer server(80);

String mockWifi = "wifi-2";
String mockBle = "ble-2";
//
//void handleHealth() {
//  DynamicJsonDocument json(1024);
//  
//  String stateValues[] = {"GREEN","YELLOW"};
//  int statePeriods[] = {120, 40};
//  double usageCpuValues[] = {50.0, 80.0};
//  int usageCpuPeriods[] = {130, 20};
//  double usageMemoryValues[] = {70.0, 90.0};
//  int usageMemoryPeriods[] = {180, 10};
//  json["state"] = stringValue(stateValues, statePeriods, 2);
//  json["usage"]["cpu"] = doubleValue(usageCpuValues, usageCpuPeriods, 2);
//  json["usage"]["memory"] = doubleValue(usageMemoryValues, usageMemoryPeriods, 2);
//
//  String response;
//  serializeJson(json, response);
//  server.send(200, "application/json" ,response.c_str());
//}

void handleHealth() {
  DynamicJsonDocument json(1024);
  
  String statusValues[] = {"OK","NOT OK"};
  int statusPeriods[] = {120, 15};
  double threadsValues[] = {400.0, 800.0};
  int threadsPeriods[] = {150, 15};
  json["application-threads"] = doubleValue(threadsValues, threadsPeriods, 2);
  json["status"] = stringValue(statusValues, statusPeriods, 2);

  String response;
  serializeJson(json, response);
  server.send(200, "application/json" ,response.c_str());
}


void connectToWifi() {
  WiFi.begin(wifiSsid.c_str(), wifiPassword.c_str());

  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
  }
  Serial.print("Wifi client IP: ");
  Serial.println(WiFi.localIP());
}

void bleServerSetup() {
  BLEDevice::init(mockBle.c_str());
  BLEServer *pServer = BLEDevice::createServer();
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->setScanResponse(true);
  BLEDevice::startAdvertising();
  Serial.println("BLE server setup completed");
}

void wifiApSetup() {
  Serial.println("Setting up Wifi Access Point");
  WiFi.softAP(mockWifi.c_str(), "notimportant123");
  IPAddress apIP = WiFi.softAPIP();
  Serial.print("Wifi AP IP: ");
  Serial.println(apIP);
}

void handleNotFound() {
  String message = "Resource Not Found.";
  server.send(404, "text/plain", message);
}

void handleCommand() {
  String message = "Got some command";
  Serial.println(message);
  server.send(200, "text/plain", message);
}

String stringValue(String values[], int periods[], int n) {
  int perdiod = checkPeriod(periods, n);
  return values[perdiod];
}

double doubleValue(double values[], int periods[], int n) {
  int perdiod = checkPeriod(periods, n);
  return values[perdiod];
}

int checkPeriod(int periods[], int n) {
  uint64_t nowSeconds = millis() / 1000;
  int periodsSum = 0;
  for (int i = 0; i < n; i++)
    periodsSum += periods[i];
  int target = nowSeconds % periodsSum;
  int period = 0;
  int offset = 0;
  while (period < n) {
    offset += periods[period];
    if (offset > target)
      break;
    period++;
  }
  return period;
}
 


void setup() {
  Serial.begin(115200);
  Serial.println("Welcome to IoT Nexum - Device Mock");
  connectToWifi();
  wifiApSetup();
  bleServerSetup();
  
  server.on("/health", HTTP_GET, handleHealth);
  server.on("/command", HTTP_POST, handleCommand);
  server.onNotFound(handleNotFound);
  server.begin();
  Serial.println("HTTP server started");
}

void loop() {
  server.handleClient();
}
