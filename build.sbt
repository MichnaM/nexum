name := "nexum"
scalaVersion := "2.13.8"

ThisBuild / dynverSeparator := "-"

Compile / mainClass := Some("nexum.NexumApp")

val Akka          = "2.6.19"
val AkkaHttp      = "10.2.9"
val AkkaHttpCirce = "1.39.2"
val Elastic4s     = "7.17.2"
val Circe         = "0.14.1"
val Scribe        = "3.8.2"
val JavaMail      = "1.4.7"
val PureConfig    = "0.17.1"
val Cats          = "2.7.0"

// akka
libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor-typed" % Akka,
  "com.typesafe.akka" %% "akka-stream" % Akka,
  "com.typesafe.akka" %% "akka-stream-testkit" % Akka % Test,
  "com.typesafe.akka" %% "akka-http" % AkkaHttp,
  "de.heikoseeberger" %% "akka-http-circe" % AkkaHttpCirce
)

//noinspection SbtDependencyVersionInspection
libraryDependencies ++= Seq(
  "com.sksamuel.elastic4s" %% "elastic4s-client-esjava" % Elastic4s,
  "com.sksamuel.elastic4s" %% "elastic4s-core" % Elastic4s,
  "com.sksamuel.elastic4s" %% "elastic4s-testkit" % Elastic4s % "test",
  "com.sksamuel.elastic4s" %% "elastic4s-json-circe" % Elastic4s
)

// circe
libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser",
  "io.circe" %% "circe-yaml"
).map(_ % Circe)

// others
libraryDependencies += "javax.mail" % "mail" % JavaMail
libraryDependencies += "com.outr" %% "scribe-slf4j" % Scribe
libraryDependencies += "com.github.pureconfig" %% "pureconfig" % PureConfig
libraryDependencies += "org.typelevel" %% "cats-core" % Cats

// docker
enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)

dockerExposedPorts ++= Seq(8081)
dockerRepository := Some("dedless")
Docker / packageName := "nexum"
Docker / version := version.value
dockerBaseImage := "openjdk:17.0.2"
dockerUpdateLatest := true
