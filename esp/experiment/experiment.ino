#include <WiFi.h>
#include <WiFiClient.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEServer.h>
#include <BLEAdvertisedDevice.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

int bleScanTime = 2; // in seconds

String nexumUrl = "https://michnam.pl/api/echo";

// Wifi connection
const char *wifiSsid = "";
const char *wifiPassword = "";

// Bluetooth
BLEScan* bleScanner;

class BleScanResult: public BLEAdvertisedDeviceCallbacks {
    void onResult(BLEAdvertisedDevice advertisedDevice) {
      String bleName = advertisedDevice.getName().c_str();
      String addr = advertisedDevice.getAddress().toString().c_str();
      int rssi = advertisedDevice.getRSSI();
    }
};

void setup() {
  Serial.begin(115200);
  delay(1000);
  Serial.println("Welcome to IoT Nexum - DeviceManager");
  wifiClientSetup();
  bluetoothClientSetup();
}

void loop() {
  delay(100);
  wifiScan();
  bleScan();
  sendHttpRequest();
}

void wifiClientSetup() {
  WiFi.begin(wifiSsid, wifiPassword);

  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
  }
  Serial.print("Wifi client IP: ");
  Serial.println(WiFi.localIP());
}

void bluetoothClientSetup() {
  bleScanner = BLEDevice::getScan(); //create new scan
  bleScanner -> setAdvertisedDeviceCallbacks(new BleScanResult());
  bleScanner -> setActiveScan(true); //active scan uses more power, but get results faster
  bleScanner -> setInterval(100);
  bleScanner -> setWindow(99);  // less or equal setInterval value
  Serial.println("Ble scan setup successful\n.");
}

void wifiScan() {
  int n = WiFi.scanNetworks();
  Serial.print("Wifi devices found: ");
  Serial.println(n);
}

void bleScan() {
  BLEScanResults foundDevices = bleScanner->start(bleScanTime, false);
  Serial.print("BLE devices found: ");
  Serial.println(foundDevices.getCount());
  bleScanner->clearResults();
}

void sendHttpRequest() {
  HTTPClient http;
  http.begin(nexumUrl);
  http.addHeader("Content-Type", "application/json");

  int httpResponseCode = http.POST("{\"msg\": \"hello from device manager\"}");
  Serial.print("HTTP Response code: ");
  Serial.println(httpResponseCode);
  http.end();

  Serial.println(".");
}
