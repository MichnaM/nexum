# Nexum

The aim of this work is to create a platform that allows monitoring devices in IoT
networks with the possibility of adding one's own devices and then monitoring the
correctness of their operation, taking into account, among others, the local media of
the device (WiFi, Bluetooth), as well as user-defined metrics, as well as controlling
them and sending notifications to the user.

During the development of the system, in addition to the web service, an
intermediate component located in the network together with the monitored devices
was used, making it possible to monitor devices in private networks. Integration with
**Elasticsearch** and **Kibana** software offers advanced analysis of data collected from
devices in real time, as well as the ability to create interactive, complex data
visualizations.

During platform development, focus was placed on automation of deployment (along
with the ability to run the platform in multiple modes), as well as clarity of its
configuration and connectivity between individual components allowing for addition
of new functionalities.

Tech Stack:
* Scala
* ESP32
* Docker
* Akka Streams
* Akka HTTP
* Elasticsearch
* Kibana

Inspired by:
* Datadog IoT
* Domotz
* OpenRemote

System architecture:
<div align="center">
<img src="/example/system_architecture.jpg"  width="600">
</div>

Default kibana dashboard:
<div align="center">
<img src="/example/dashboard.png"  width="600">
</div>
