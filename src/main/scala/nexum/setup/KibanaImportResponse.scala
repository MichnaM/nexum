package nexum.setup

import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder, Json}

case class KibanaImportResponse(
  successCount: Int,
  success:      Boolean,
  warnings:     List[String],
  errors:       Option[Json]
) extends FailFastCirceSupport

object KibanaImportResponse {
  implicit val kibanaImportResponseDecoder: Decoder[KibanaImportResponse] = deriveDecoder[KibanaImportResponse]
  implicit val kibanaImportResponseEncoder: Encoder[KibanaImportResponse] = deriveEncoder[KibanaImportResponse]
}
