package nexum.setup

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.Multipart.FormData
import akka.http.scaladsl.model.Uri.Path
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest, HttpResponse, StatusCode, headers}
import akka.http.scaladsl.model.headers.{Authorization, BasicHttpCredentials}
import cats.syntax.either._
import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.requests.indexes.{CreateIndexRequest, IndexSettings}
import com.sksamuel.elastic4s.requests.mappings.MappingDefinition
import io.circe.Json
import io.circe.jawn.decode
import io.circe.yaml._
import nexum.EsIndices._
import nexum.GlobalContext
import nexum.domain.User

import java.io.{BufferedWriter, File, FileWriter}
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps
import scala.util.{Failure, Success}

class SetupRunner(ctx: GlobalContext)(implicit actorSystem: ActorSystem[_]) {

  def run(): Unit = {
    if (ctx.config.setup.indices) indicesSetupRun()
    if (ctx.config.setup.indexPatterns) indexPatternsSetupRun()
    if (ctx.config.setup.dashboards) dashboardsSetupRun()
    if (ctx.config.setup.admin) userSetupRun()
  }

  private def indicesSetupRun(): Unit = {
    val indices = scala.io.Source.fromResource(s"elasticsearch/indices.csv").getLines.toList.map(_.split(",").toList)

    val mappingFutures = indices.map { indexCfg =>
      val settings = new IndexSettings
      settings.shards   = indexCfg(2).toInt
      settings.replicas = indexCfg(3).toInt
      (
        indexCfg.head,
        ctx.esClient.createIndex(
          CreateIndexRequest(
            name     = indexCfg.head,
            mapping  = Some(MappingDefinition(rawSource = Some(loadMapping(indexCfg.head).toString))),
            settings = settings
          )
        )
      )
    }

    mappingFutures.foreach {
      case (indexName, future) =>
        Await.ready(future, 1.minute).onComplete {
          case Success(_) => scribe.info(s"Created mapping: $indexName")
          case Failure(error) => scribe.warn(error.getMessage)
        }
    }
  }

  private def indexPatternsSetupRun(): Unit = {
    val indices    = scala.io.Source.fromResource(s"elasticsearch/indices.csv").getLines.toList.map(_.split(",").toList)
    val authKibana = Authorization(BasicHttpCredentials(ctx.config.kibana.username, ctx.config.kibana.password))

    val indexPatternsFutures = indices.map { indexCfg =>
      (
        indexCfg.head,
        httpPostIndexPattern(indexCfg.head, indexCfg(1), authKibana)
      )

    }

    indexPatternsFutures.foreach {
      case (indexName, future) =>
        Await.ready(future, 1.minute).onComplete {
          case Success(response) if response.status == StatusCode.int2StatusCode(200) =>
            scribe.info(s"Created kibana index pattern: $indexName")
          case Success(response) =>
            scribe.warn(
              s"Failed creating kibana index pattern: $indexName, fault: ${response.entity.toStrict(1.second).map(_.data.utf8String)}"
            )
          case Failure(error) => scribe.warn(error.getMessage)
        }
    }
  }

  private def dashboardsSetupRun(): Unit = {
    val dashboardsFile = new File("dashboards/dashboards.ndjson")
    if (dashboardsFile.isFile) {
      val auth           = Authorization(BasicHttpCredentials(ctx.config.kibana.username, ctx.config.kibana.password))
      val responseFuture = Http().singleRequest(
        HttpRequest(
          method  = HttpMethods.POST,
          uri     = ctx.config.kibana.server + "/api/saved_objects/_import",
          headers = List(auth, headers.RawHeader("kbn-xsrf", "true")),
          entity  =
            FormData.fromFile(
              "file",
              ContentTypes.`application/octet-stream`,
              dashboardsFile
            ).toEntity
        )
      )
        .flatMap(_.entity.toStrict(1.second).map(_.data.utf8String).map(decode[KibanaImportResponse](_)))
        .map {
          case Left(error) => scribe.error(s"Dashboard setup - Error parsing response from Kibana api: $error")
          case Right(res) =>
            (res.success, res.warnings.size) match {
              case (true, 0) => scribe.info(s"Dashboard setup - Imported kibana ${res.successCount} dashboards")
              case (true, _) => scribe.warn(s"Dashboard setup - Finished dashboards import with warnings: ${res.warnings}")
              case _ =>
                scribe.warn(
                  s"Dashboard setup - Error importing dashboards: ${res.errors.getOrElse(Json.fromString("")).noSpaces})"
                )
            }
        }

      Await.ready(responseFuture, 1.minute).onComplete {
        case Failure(error) => scribe.warn(error.getMessage)
        case _ =>
      }
    } else {
      scribe.warn("Dashboards setup enabled, but not dashboards.ndjson file provided, skipping...")
    }

  }

  private def userSetupRun(): Unit = {
    val userFuture =
      ctx.esClient.execIndex(indexInto(nexum_users).doc(User("admin", Some("nexum"))).id("admin").refreshImmediately)
    Await.ready(userFuture, 1.minute).onComplete {
      case Success(_) => scribe.info(s"Created or updated user admin")
      case Failure(error) => scribe.warn(error.getMessage)
    }
  }

  private def httpPostIndexPattern(title: String, timeFieldName: String, auth: Authorization): Future[HttpResponse] = {
    val entity = HttpEntity(
      contentType = ContentTypes.`application/json`,
      string      = timeFieldName match {
        case "" =>
          s"""{
             |"index_pattern": {
             |   "id": "$title",
             |   "title" : "$title"
             |}
             |}""".stripMargin
        case _ =>
          s"""{
             |"index_pattern": {
             |   "id": "$title",
             |   "title" : "$title",
             |   "timeFieldName": "$timeFieldName"
             |}
             |}""".stripMargin
      }
    )

    Http().singleRequest(
      HttpRequest(
        method  = HttpMethods.POST,
        uri     = ctx.config.kibana.server + "/api/index_patterns/index_pattern",
        headers = List(auth, headers.RawHeader("kbn-xsrf", "true")),
        entity  = entity
      )
    )
  }

  private def loadMapping(mappingName: String): Json = {
    try {
      val mappingYaml = scala.io.Source.fromResource(s"elasticsearch/mappings/$mappingName.yaml").mkString
      val mappingJson: Json = parser.parse(mappingYaml).valueOr(throw _)
      mappingJson
    } catch {
      case e: Exception =>
        scribe.error(s"Could not parse mapping $mappingName: ${e.getMessage}")
        throw e
    }
  }

}

object SetupRunner {
  def apply(ctx: GlobalContext)(implicit actorSystem: ActorSystem[_]): Unit = new SetupRunner(ctx).run()
}
