package nexum

case class Config(
  setup:                        SetupConfig,
  logging:                      Logging,
  elasticsearch:                ElasticsearchConfig,
  kibana:                       Kibana,
  notifications:                Notifications,
  esAwaitReadiness:             Boolean,
  kibanaAwaitReadiness:         Boolean,
  monitoringProcessorEnable:    Boolean,
  notificationsProcessorEnable: Boolean,
  fetchStatusTime:              Double,
  monitoringFlowInterval:       Double,
  stateUpdateMinimalInterval:   Double
)

case class SetupConfig(
  indices:       Boolean,
  indexPatterns: Boolean,
  dashboards:    Boolean,
  admin:         Boolean
)

case class Logging(
  level: String
)

case class ElasticsearchConfig(
  server:   String,
  username: String,
  password: String
)

case class Kibana(
  server:   String,
  username: String,
  password: String
)

case class Notifications(
  host:     String,
  sender:   String,
  password: String,
  maxAge:   Double
)
