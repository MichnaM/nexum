package nexum

import nexum.lib.es.ElasticsearchClient
import nexum.lib.mail.MailClient

import javax.mail.Session

case class GlobalContext(config: Config) {

  val esClient: ElasticsearchClient = ElasticsearchClient(config.elasticsearch)

}
