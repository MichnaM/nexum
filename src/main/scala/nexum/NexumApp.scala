package nexum

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import nexum.processor.monitoring.MonitoringProcessor
import nexum.http.route.{DevicemanagerRoutes, UserRoutes}
import nexum.lib.logging.ScribeLogging
import nexum.processor.notification.NotificationProcessor
import nexum.setup.SetupRunner
import pureconfig.ConfigSource
import pureconfig.generic.auto._

object NexumApp extends App with FailFastCirceSupport {

  implicit val actorSystem: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "IoTNexum")

  scribe.info("Welcome to Nexum - IoT Monitoring Platform")

  val config: Config = ConfigSource.default.load[Config] match {
    case Right(conf) => conf
    case Left(failure) => sys.error(failure.toString)
  }

  ScribeLogging(config)
  implicit val ctx: GlobalContext = GlobalContext(config)

  if (config.esAwaitReadiness) waitForEsConnection(ctx)
  if (config.kibanaAwaitReadiness) waitForKibanaConnection(ctx)
  SetupRunner(ctx)

  val route: Route = Route.seal {
    DevicemanagerRoutes.routes ~
      UserRoutes.routes
  }

  Http().newServerAt("0.0.0.0", 8081).bind(route)

  if (config.monitoringProcessorEnable) new MonitoringProcessor().run()
  if (config.notificationsProcessorEnable) new NotificationProcessor().run()
}
