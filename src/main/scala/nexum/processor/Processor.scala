package nexum.processor

import akka.Done
import akka.stream.RestartSettings
import akka.stream.scaladsl.{RestartSource, Source}
import nexum.NexumApp.actorSystem

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt

trait Processor {

  val restartSettings: RestartSettings = RestartSettings(
    minBackoff   = 1.seconds,
    maxBackoff   = 10.seconds,
    randomFactor = 0.2
  ).withMaxRestarts(10, 1.minute)

  val streamDefinition: Source[_, _]

  def run(): Future[Done] = {
    RestartSource.onFailuresWithBackoff(restartSettings)(() => streamDefinition).run()
  }
}
