package nexum.processor.monitoring.flow

import akka.NotUsed
import akka.stream.scaladsl.Flow
import nexum.GlobalContext
import nexum.domain.device.Device
import nexum.service.DeviceManagerService

import scala.concurrent.ExecutionContext.Implicits.global

object FetchDeviceManagerIDs {

  def apply(implicit ctx: GlobalContext): Flow[Unit, String, NotUsed] = {
    Flow[Unit].map { _ =>
      scribe.debug("Starting device monitoring run")
      DeviceManagerService.getAllDeviceManagers(ctx.esClient).map {
        case Some(dms) => dms.filter(_.token != "").map(_.dmID)
        case None => Nil
      }

    }
      .mapAsync(1)(identity)
      .mapConcat(identity)
  }

}
