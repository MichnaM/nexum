package nexum.processor.monitoring.flow

import akka.NotUsed
import akka.stream.scaladsl.Flow
import nexum.GlobalContext
import nexum.domain.device.Device
import nexum.service.DeviceService

import scala.concurrent.ExecutionContext.Implicits.global

object FetchDevicesFlow {

  def apply(implicit ctx: GlobalContext): Flow[String, Device, NotUsed] = {
    Flow[String].map { dmID =>
      DeviceService.getDevicesByDmID(dmID)(ctx.esClient).map {
        case Some(devices) =>
          scribe.debug(s"Monitoring - fetched ${devices.length} devices for dm $dmID")
          devices
        case None => Nil
      }
    }
      .mapAsync(1)(identity)
      .mapConcat(identity)
  }

}
