package nexum.processor.monitoring.flow

import akka.NotUsed
import akka.stream.scaladsl.Flow
import nexum.GlobalContext
import nexum.domain.device.Device
import nexum.service.DeviceDataPortionsService

import scala.concurrent.ExecutionContext.Implicits.global

object MergeDataPortionsIntoDeviceFlow {

  def apply(implicit ctx: GlobalContext): Flow[Device, Device, NotUsed] = {
    Flow[Device].map { device =>
      val maxPeriod = device.config.getMaxPeriod
      DeviceDataPortionsService.getDataPortionsByDeviceID(device.deviceID, maxPeriod)(ctx.esClient).map {
        case Some(portions) =>
          Device.insertDataPortions(device, Some(portions))
        case None => Device.insertDataPortions(device, Some(Nil))
      }
    }.mapAsync(8)(identity)
  }

}
