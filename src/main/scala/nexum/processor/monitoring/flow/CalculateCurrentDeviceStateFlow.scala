package nexum.processor.monitoring.flow

import akka.NotUsed
import akka.http.scaladsl.model.DateTime
import akka.stream.scaladsl.Flow
import nexum.GlobalContext
import nexum.domain.device.State.{State, green, red, unknown}
import nexum.domain.device.metric.{DoubleMetric, StringMetric}
import nexum.domain.device.{Device, DeviceState, DoubleDataPortion, State, StringDataPortion}
import nexum.domain.devicemanager.DeviceDataPortion

object CalculateCurrentDeviceStateFlow {

  def apply(implicit ctx: GlobalContext): Flow[Device, DeviceState, NotUsed] = {
    Flow[Device].map { device =>
      val now = DateTime.now

      val dmMetricsResList = List(checkBtMetric(device), checkWifiMetric(device), checkLatencyMetric(device))
      val dmMetricsRes     = checkStateList(dmMetricsResList)

      val doubleMetricsResList = checkDoubleMetrics(device, now)
      val stringMetricsResList = checkStringMetrics(device, now)

      val doubleMetricsRes = checkStateList(doubleMetricsResList)
      val stringMetricsRes = checkStateList(stringMetricsResList)

      val res = checkStateList(dmMetricsRes :: doubleMetricsRes :: stringMetricsRes :: Nil)

      if (res != State.green)
        scribe.info(s"Device: ${device.deviceID} - calculated state: $res" +
          s" - metrics dm: $dmMetricsRes, string: $stringMetricsRes, double: $doubleMetricsRes")

      DeviceState.fromDeviceData(device, res, now)
    }

  }

  private def checkDoubleMetrics(device: Device, now: DateTime): List[State] = {
    device.config.doubleMetrics.map { metric =>
      val timedDeviceDataPortions =
        filterDeviceDataPortionsByTime(device.deviceData.get, metric.period, now).flatMap { deviceDataPortion =>
          deviceDataPortion.doubleData.find(_.name == metric.name)
        }
      checkSingleDoubleMetric(metric, timedDeviceDataPortions)
    }
  }

  private def checkStringMetrics(device: Device, now: DateTime): List[State] = {
    device.config.stringMetrics.map { metric =>
      val timedDeviceDataPortions =
        filterDeviceDataPortionsByTime(device.deviceData.get, metric.period, now).flatMap { deviceDataPortion =>
          deviceDataPortion.stringData.find(_.name == metric.name)
        }
      checkSingleStringMetric(metric, timedDeviceDataPortions)
    }
  }

  private def checkSingleDoubleMetric(metric: DoubleMetric, dataPortions: List[DoubleDataPortion]): State = {
    if (dataPortions.nonEmpty) {
      val metricCheckResults = dataPortions.map { portion =>
        portion.value match {
          case Some(value) => value > metric.valueGrater && value < metric.valueLess
          case None => false
        }
      }

      val passRatio = metricCheckResults.count(_ == true).toDouble / metricCheckResults.length.toDouble
      if (passRatio > metric.passMinRatio) green else red
    } else {
      unknown
    }
  }

  private def checkSingleStringMetric(metric: StringMetric, dataPortions: List[StringDataPortion]): State = {
    if (dataPortions.nonEmpty) {
      val metricCheckResults = dataPortions.map { portion =>
        portion.value match {
          case Some(value) => metric.valueIn.contains(value)
          case None => false
        }
      }

      val passRatio = metricCheckResults.count(_ == true).toDouble / metricCheckResults.length.toDouble
      if (passRatio > metric.passMinRatio) green else red
    } else {
      unknown
    }

  }

  private def filterDeviceDataPortionsByTime(
    deviceDataPortions: List[DeviceDataPortion],
    age:                Double,
    now:                DateTime
  ): List[DeviceDataPortion] =
    deviceDataPortions.filter(_.timestamp.clicks > now.minus((age * 1000).toLong).clicks)

  private def checkBtMetric(device: Device): State = {
    if (device.config.btMetric.enabled)
      checkSingleDoubleMetric(
        device.config.btMetric,
        device.deviceData.get.map(_.wifiData)
      )
    else green
  }

  private def checkWifiMetric(device: Device): State = {
    if (device.config.wifiMetric.enabled)
      checkSingleDoubleMetric(
        device.config.wifiMetric,
        device.deviceData.get.map(_.wifiData)
      )
    else green
  }

  private def checkLatencyMetric(device: Device): State = {
    if (device.config.latencyMetric.enabled)
      checkSingleDoubleMetric(
        device.config.latencyMetric,
        device.deviceData.get.map(_.latencyData)
      )
    else green
  }

  private def checkStateList(states: List[State]): State = {
    if (states.contains(red)) red
    else if (states.contains(unknown)) unknown
    else green
  }

}
