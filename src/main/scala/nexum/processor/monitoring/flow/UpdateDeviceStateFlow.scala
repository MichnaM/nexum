package nexum.processor.monitoring.flow

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.sksamuel.elastic4s.requests.update.UpdateRequest
import nexum.EsIndices.nexum_devices
import nexum.GlobalContext
import nexum.domain.device.DeviceState
import nexum.lib.es.ElasticResult
import nexum.service.{DeviceService, DeviceStateService}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object UpdateDeviceStateFlow {

  def apply(implicit ctx: GlobalContext): Flow[DeviceState, Option[ElasticResult], NotUsed] = {
    Flow[DeviceState].map { deviceState =>
      DeviceStateService.getLatestStateByDeviceID(deviceState.deviceID)(ctx.esClient).map {
        case Some(prevState) =>
          if (
            deviceState.state != prevState.state &&
            checkChangeInterval(
              deviceState,
              prevState,
              ctx.config.stateUpdateMinimalInterval
            )
          ) {
            Some(deviceState)
          } else {
            None
          }
        case None => Some(deviceState)
      }.flatMap {
        case Some(deviceState) =>
          scribe.info(s"Device: ${deviceState.deviceID} - changed state to ${deviceState.state}")
          DeviceStateService.indexDeviceState(deviceState)(ctx.esClient).flatMap { _ =>
            try {
              ctx.esClient.execUpdate(UpdateRequest(nexum_devices, deviceState.deviceID).doc("state" -> deviceState.state)).map {
                response =>
                  Some(ElasticResult(response.result))
              }
            } catch {
              case _: Throwable =>
                scribe.info(s"Error updating device state in ES")
                Future.successful(None)
            }
          }
        case None => Future.successful(None)
      }
    }.mapAsync(8)(identity)
  }

  def checkChangeInterval(current: DeviceState, prev: DeviceState, changeInterval: Double): Boolean =
    current.timestamp.minus((changeInterval * 1000).toLong).clicks > prev.timestamp.clicks

}
