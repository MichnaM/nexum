package nexum.processor.monitoring

import akka.stream.scaladsl.Source
import nexum.GlobalContext
import nexum.processor.Processor
import nexum.processor.monitoring.flow.{
  CalculateCurrentDeviceStateFlow,
  FetchDeviceManagerIDs,
  FetchDevicesFlow,
  MergeDataPortionsIntoDeviceFlow,
  UpdateDeviceStateFlow
}

import scala.concurrent.duration.{DurationDouble, DurationInt}

class MonitoringProcessor(implicit ctx: GlobalContext) extends Processor {

  override val streamDefinition: Source[_, _] =
    Source.tick(1.second, ctx.config.monitoringFlowInterval.seconds, scribe.info("Starting monitoring processor"))
      .via(FetchDeviceManagerIDs(ctx))
      .via(FetchDevicesFlow(ctx))
      .via(MergeDataPortionsIntoDeviceFlow(ctx))
      .via(CalculateCurrentDeviceStateFlow(ctx))
      .via(UpdateDeviceStateFlow(ctx))

}
