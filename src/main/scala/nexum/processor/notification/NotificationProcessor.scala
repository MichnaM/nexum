package nexum.processor.notification

import akka.stream.scaladsl.Source
import nexum.GlobalContext
import nexum.processor.Processor
import nexum.processor.notification.flow.{FetchConfigAndStates, NotificationCalculationFlow, NotificationProcessingFlow}

import scala.concurrent.duration.{DurationDouble, DurationInt}

class NotificationProcessor(implicit ctx: GlobalContext) extends Processor {

  override val streamDefinition: Source[_, _] =
    Source.tick(5.second, ctx.config.monitoringFlowInterval.seconds, scribe.info("Starting notifications processor"))
      .via(FetchConfigAndStates(ctx))
      .via(NotificationCalculationFlow(ctx))
      .via(NotificationProcessingFlow(ctx))

}
