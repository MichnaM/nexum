package nexum.processor.notification.flow

import akka.NotUsed
import akka.stream.scaladsl.Flow
import nexum.GlobalContext
import nexum.domain.device.DeviceState
import nexum.domain.notification.Notification
import nexum.processor.notification.model.NotificationData

object NotificationCalculationFlow {
  def apply(implicit ctx: GlobalContext): Flow[(List[Notification], List[DeviceState]), NotificationData, NotUsed] = {
    Flow[(List[Notification], List[DeviceState])].map {
      case (notifications, deviceStates) =>
        notifications.map { notification =>
          val statesToNotify = deviceStates.filter(d => notification.deviceIDs.contains(d.deviceID))
          NotificationData(notification, statesToNotify)
        }
    }.mapConcat(identity)
  }
}
