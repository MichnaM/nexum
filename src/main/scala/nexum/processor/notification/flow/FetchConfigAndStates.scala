package nexum.processor.notification.flow

import akka.NotUsed
import akka.stream.scaladsl.Flow
import nexum.GlobalContext
import nexum.domain.device.DeviceState
import nexum.domain.notification.Notification
import nexum.service.{DeviceStateService, NotificationService}
import scala.concurrent.ExecutionContext.Implicits.global

object FetchConfigAndStates {

  def apply(implicit ctx: GlobalContext): Flow[Unit, (List[Notification], List[DeviceState]), NotUsed] = {
    Flow[Unit].map { _ =>
      scribe.debug("Starting notification processing run")
      val deviceStatesFuture  = DeviceStateService.getNotNotified(ctx.config.notifications.maxAge)(ctx.esClient)
      val notificationsFuture = NotificationService.getNotifications(ctx.esClient)

      for {
        notifications <- notificationsFuture
        deviceStates <- deviceStatesFuture
      } yield (notifications.getOrElse(Nil), deviceStates.getOrElse(Nil))
    }
      .mapAsync(1)(identity)
  }
}
