package nexum.processor.notification.flow

import akka.NotUsed
import akka.stream.scaladsl.Flow
import nexum.GlobalContext
import nexum.domain.device.DeviceState
import nexum.lib.es.ElasticResult
import nexum.lib.mail.MailService
import nexum.processor.notification.model.NotificationData
import nexum.service.DeviceStateService

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object NotificationProcessingFlow {

  def apply(implicit ctx: GlobalContext): Flow[NotificationData, Option[ElasticResult], NotUsed] = {
    Flow[NotificationData].map { notiData =>
      notiData.states.size match {
        case 0 => Future.successful(None)
        case _ =>
          val mailSubject = "IoT Nexum - Device states update"
          val mailMessage = generateMailMessage(notiData.states)
          try {
            MailService.sendDeviceStateMail(mailSubject, mailMessage, notiData.notification.receivers, ctx.config.notifications)
            DeviceStateService.confirmNotified(notiData.states.map(_._id.get))(ctx.esClient).map {
              case Some(response) => Some(ElasticResult(response.toString))
              case None => None
            }
          } catch {
            case e: Throwable =>
              scribe.error(s"Error processing mail notification: ${e.getMessage}")
              Future.successful(None)
          }
      }
    }.mapAsync(1)(identity)

  }

  private def generateMailMessage(states: List[DeviceState]): String = {
    val stateInfo = states
      .sortWith(_.timestamp < _.timestamp)
      .map(state => s"${state.timestamp} - Device: ${state.deviceID} changed state to ${state.state}")

    ("Devices that changed state:\n" :: stateInfo).mkString("\n")
  }
}
