package nexum.processor.notification.model

import nexum.domain.device.DeviceState
import nexum.domain.notification.Notification

case class NotificationData(notification: Notification, states: List[DeviceState])
