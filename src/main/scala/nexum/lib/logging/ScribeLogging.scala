package nexum.lib.logging

import nexum.Config
import scribe.Level
import scribe.format.{FormatterInterpolator, classNameSimple, date, levelColoredPaddedRight, mdc, messages}

object ScribeLogging {
  def apply(config: Config): Unit = {
    val formatter = formatter"$date $levelColoredPaddedRight $classNameSimple - $messages $mdc"

    scribe.Logger.root
      .clearHandlers()
      .clearModifiers()
      .withHandler(
        minimumLevel = Some(Level(config.logging.level)),
        formatter    = formatter
      )
      .replace()
  }
}
