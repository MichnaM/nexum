package nexum.lib.es

import akka.Done
import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.requests.get.{GetRequest, GetResponse, MultiGetResponse}
import com.sksamuel.elastic4s.requests.searches.{SearchRequest, SearchResponse}
import com.sksamuel.elastic4s.{Indexes, RequestFailure, RequestSuccess}

import scala.concurrent.Future

trait ReadApi {
  this: ElasticsearchClient =>

  def execGet(getRequest: GetRequest): Future[GetResponse] = {
    client.execute(getRequest).map {
      case rs @ RequestSuccess(_, _, _, result) =>
        checkResponseSuccess(rs)
        result
      case failure: RequestFailure =>
        throw ElasticException(s"execGet-${getRequest.index}, id: ${getRequest.id}", failure.error)
    }
  }

  def execMultiGet(getRequests: List[GetRequest]): Future[MultiGetResponse] = {
    val multiGetRequest = multiget(getRequests)
    val indices         = multiGetRequest.gets.map(_.index.index).distinct.sorted.mkString(",")

    client.execute(multiGetRequest).map {
      case rs @ RequestSuccess(_, _, _, result) =>
        checkResponseSuccess(rs)
        result
      case failure: RequestFailure =>
        throw ElasticException(s"execMultiGet-$indices", failure.error)
    }
  }

  def execSearch(searchRequest: SearchRequest, refreshBefore: Boolean = false): Future[SearchResponse] = {
    val indices = searchRequest.indexes.values.distinct.sorted.mkString(",")

    val refreshFuture = if (refreshBefore) refreshIndices(indices) else Future.successful(Done)
    refreshFuture.flatMap { _ =>
      client.execute(searchRequest).map {
        case rs @ RequestSuccess(_, _, _, result) =>
          checkResponseSuccess(rs)
          result
        case failure: RequestFailure =>
          throw ElasticException(s"execSearch-$indices", failure.error)
      }
    }
  }

  def refreshIndices(indices: Indexes): Future[Done.type] = {
    client.execute(refreshIndex(indices)).map { _ =>
      Done
    }
  }

}
