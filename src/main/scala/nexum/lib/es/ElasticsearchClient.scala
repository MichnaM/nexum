package nexum.lib.es

import akka.http.scaladsl.model.StatusCodes
import com.sksamuel.elastic4s.http.JavaClient
import com.sksamuel.elastic4s.{ElasticClient, ElasticProperties, RequestSuccess}
import nexum.ElasticsearchConfig
import org.apache.http.auth.{AuthScope, UsernamePasswordCredentials}
import org.apache.http.client.config.RequestConfig
import org.apache.http.impl.client.BasicCredentialsProvider
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder
import org.elasticsearch.client.RestClientBuilder
import org.elasticsearch.client.RestClientBuilder.{HttpClientConfigCallback, RequestConfigCallback}

import scala.collection.concurrent.TrieMap
import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.global

class ElasticsearchClient(esConfig: ElasticsearchConfig) extends ReadApi with WriteApi with AdminApi {
  implicit val executionContext: ExecutionContext = global

  private val credsProvider = {
    val provider = new BasicCredentialsProvider()
    provider.setCredentials(
      AuthScope.ANY,
      new UsernamePasswordCredentials(esConfig.username, esConfig.password)
    )
    provider
  }

  private val httpClientConfigCallback = new HttpClientConfigCallback {
    override def customizeHttpClient(httpClientBuilder: HttpAsyncClientBuilder): HttpAsyncClientBuilder =
      httpClientBuilder
        .setKeepAliveStrategy((_, _) => 500 * 1000)
        .setDefaultCredentialsProvider(credsProvider)
  }

  private val requestConfigCallback = new RequestConfigCallback {
    override def customizeRequestConfig(requestConfigBuilder: RequestConfig.Builder): RequestConfig.Builder = requestConfigBuilder
      .setSocketTimeout(RestClientBuilder.DEFAULT_SOCKET_TIMEOUT_MILLIS * 5)
      .setConnectTimeout(RestClientBuilder.DEFAULT_CONNECT_TIMEOUT_MILLIS * 200)
      .setConnectionRequestTimeout(RestClientBuilder.DEFAULT_CONNECT_TIMEOUT_MILLIS * 200)
  }

  lazy val client: ElasticClient = ElasticClient(
    JavaClient(
      ElasticProperties(esConfig.server),
      requestConfigCallback    = requestConfigCallback,
      httpClientConfigCallback = httpClientConfigCallback
    )
  )

  def checkResponseSuccess(requestSuccess: RequestSuccess[_]): Unit = {
    val isFailed = StatusCodes
      .getForKey(requestSuccess.status)
      .exists(code => code.isFailure && code.intValue != 404)

    if (isFailed) sys.error(s"Es returned error ${requestSuccess.status} - ${requestSuccess.body.getOrElse("")}")
  }
}

object ElasticsearchClient {
  private val clients: TrieMap[ElasticsearchConfig, ElasticsearchClient] = TrieMap.empty

  def apply(esConfig: ElasticsearchConfig): ElasticsearchClient =
    clients.getOrElseUpdate(esConfig, new ElasticsearchClient(esConfig))
}
