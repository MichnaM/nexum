package nexum.lib.es

import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

case class ElasticResult(result: String) extends FailFastCirceSupport

object ElasticResult {
  implicit val elasticResultDecoder: Decoder[ElasticResult] = deriveDecoder[ElasticResult]
  implicit val elasticResultEncoder: Encoder[ElasticResult] = deriveEncoder[ElasticResult]
}
