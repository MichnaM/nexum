package nexum.lib.es

import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.requests.cluster.ClusterStateResponse
import com.sksamuel.elastic4s.requests.indexes.{CreateIndexRequest, CreateIndexResponse}
import com.sksamuel.elastic4s.requests.security.roles.{CreateRoleResponse, GlobalPrivileges, IndexPrivileges}
import com.sksamuel.elastic4s.requests.security.users.{CreateUserResponse, PlaintextPassword}
import com.sksamuel.elastic4s.{RequestFailure, RequestSuccess, Response}
import nexum.ElasticsearchConfig

import scala.concurrent.Future

trait AdminApi {
  this: ElasticsearchClient =>

  def clusterInfo: Future[ClusterStateResponse] = {
    client.execute(clusterState).map {
      case failure: RequestFailure => throw ElasticException("clusterInfo", failure.error)
      case RequestSuccess(_, _, _, result) => result
    }
  }

  def createIndex(createIndexRequest: CreateIndexRequest): Future[CreateIndexResponse] = {
    client.execute(createIndexRequest).map {
      case failure: RequestFailure => throw ElasticException(s"createIndex-${createIndexRequest.name}", failure.error)
      case RequestSuccess(_, _, _, result) => result
    }
  }

  def addUser(config: ElasticsearchConfig, roles: List[String]): Future[CreateUserResponse] = {
    client
      .execute(createUser(
        name     = config.username,
        password = PlaintextPassword(config.password),
        roles    = roles
      ))
      .map {
        case failure: RequestFailure => throw ElasticException("createAppUser", failure.error)
        case RequestSuccess(_, _, _, result) => result
      }
  }

  def addRole(
    name:               String,
    clusterPermissions: List[String],
    globalPrivileges:   Option[GlobalPrivileges] = None,
    indices:            List[IndexPrivileges]
  ): Future[Response[CreateRoleResponse]] =
    client.execute(createRole(
      name               = name,
      clusterPermissions = clusterPermissions,
      global             = globalPrivileges,
      indices            = indices
    ))
}
