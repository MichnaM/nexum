package nexum.lib.es

import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.requests.bulk.{BulkRequest, BulkResponse}
import com.sksamuel.elastic4s.requests.delete.{DeleteByIdRequest, DeleteResponse}
import com.sksamuel.elastic4s.requests.indexes.{IndexRequest, IndexResponse}
import com.sksamuel.elastic4s.requests.update.{UpdateByQueryRequest, UpdateByQueryResponse, UpdateRequest, UpdateResponse}
import com.sksamuel.elastic4s.{RequestFailure, RequestSuccess}

import scala.concurrent.Future

trait WriteApi {
  this: ElasticsearchClient =>

  def execIndex(indexRequest: IndexRequest): Future[IndexResponse] = {
    client.execute(indexRequest).map {
      case rs @ RequestSuccess(_, _, _, result) =>
        checkResponseSuccess(rs)
        result
      case failure: RequestFailure => throw ElasticException(s"execIndex - index: ${indexRequest.index.name}", failure.error)
    }
  }

  def execIndexBulk(bulkRequest: BulkRequest): Future[BulkResponse] = {
    val indices = bulkRequest.requests
      .collect {
        case i: IndexRequest => i.index.index
        case u: UpdateRequest => u.index.index
        case d: DeleteByIdRequest => d.index.index
      }
      .distinct
      .sorted
      .mkString(",")

    client.execute(bulkRequest).map {
      case rs @ RequestSuccess(_, _, _, result) =>
        checkResponseSuccess(rs)
        result
      case failure: RequestFailure => throw ElasticException(s"execIndexBulk - indices: $indices", failure.error)
    }
  }

  def execUpdate(updateRequest: UpdateRequest): Future[UpdateResponse] = {
    client.execute(updateRequest).map {
      case rs @ RequestSuccess(_, _, _, result) =>
        checkResponseSuccess(rs)
        result
      case failure: RequestFailure => throw ElasticException(s"execUpdate - index: ${updateRequest.index.name}", failure.error)
    }
  }

  def execDelete(deleteRequest: DeleteByIdRequest): Future[DeleteResponse] = {
    client.execute(deleteRequest).map {
      case rs @ RequestSuccess(_, _, _, result) =>
        checkResponseSuccess(rs)
        result
      case failure: RequestFailure => throw ElasticException(s"execDelete - index: ${deleteRequest.index.name}", failure.error)
    }
  }

  def updateQuery(updateRequest: UpdateByQueryRequest): Future[UpdateByQueryResponse] = {
    client.execute(updateRequest).map {
      case rs @ RequestSuccess(_, _, _, result) =>
        checkResponseSuccess(rs)
        result
      case failure: RequestFailure =>
        val indices = updateRequest.indexes.values.mkString(",")
        throw ElasticException(s"updateQuery - indices: $indices", failure.error)
    }
  }

}
