package nexum.lib.es

import com.sksamuel.elastic4s.ElasticError

case class ElasticException(ctx: String, error: ElasticError) extends RuntimeException {
  private val reasons = (error.reason :: error.rootCause.map(_.reason).toList).distinct.mkString(", ")
  override def getMessage: String = s"$ctx: $reasons"
}
