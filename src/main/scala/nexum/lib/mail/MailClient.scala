package nexum.lib.mail

import nexum.{Config, Notifications}

import java.util.Properties
import javax.mail.{Authenticator, PasswordAuthentication, Session}

object MailClient {
  def apply(notificationsConfig: Notifications): Session = {
    val props: Properties = System.getProperties
    props.put("mail.smtp.host", notificationsConfig.host)
    props.put("mail.smtp.port", "587")
    props.put("mail.smtp.auth", "true")
    props.put("mail.smtp.starttls.enable", "true")
    props.put("mail.smtp.ssl.protocols", "TLSv1.2")
    Session.getInstance(
      props,
      new Authenticator {
        override def getPasswordAuthentication: PasswordAuthentication =
          new PasswordAuthentication(notificationsConfig.sender, notificationsConfig.password)
      }
    )
  }

}
