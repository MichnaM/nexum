package nexum.lib.mail

import nexum.{Config, Notifications}

import javax.mail.{Message, Transport}
import javax.mail.internet.{InternetAddress, MimeMessage}

object MailService {

  def sendDeviceStateMail(subject: String, msg: String, recipients: List[String], notificationsConfig: Notifications): Unit = {
    val message: Message = new MimeMessage(MailClient(notificationsConfig))
    message.setFrom(new InternetAddress(notificationsConfig.sender))
    message.setRecipients(Message.RecipientType.TO, recipients.toArray.map(new InternetAddress(_)))
    message.setSubject(subject)
    message.setText(msg)
    Transport.send(message)
  }

}
