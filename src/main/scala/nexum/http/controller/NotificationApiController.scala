package nexum.http.controller

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives.{complete, onComplete}
import akka.http.scaladsl.server.Route
import nexum.GlobalContext
import nexum.domain.notification.Notification
import nexum.service.NotificationService

import scala.util.{Failure, Success}

object NotificationApiController {
  def getNotification(id: String)(implicit ctx: GlobalContext): Route = {
    val responseFuture = NotificationService.getNotification(id)(ctx.esClient)

    onComplete(responseFuture) {
      case Failure(_) => complete(StatusCodes.InternalServerError)
      case Success(result) => result match {
          case Some(cfg) => complete(cfg)
          case None => complete(StatusCodes.NotFound)
        }
    }
  }

  def getNotifications(implicit ctx: GlobalContext): Route = {
    val responseFuture = NotificationService.getNotifications(ctx.esClient)

    onComplete(responseFuture) {
      case Failure(_) => complete(StatusCodes.InternalServerError)
      case Success(result) => complete(result)
    }
  }

  def indexNotification(notification: Notification)(implicit ctx: GlobalContext): Route = {
    val responseFuture = NotificationService.indexNotification(notification)(ctx.esClient)

    onComplete(responseFuture) {
      case Failure(_) => complete(StatusCodes.InternalServerError)
      case Success(result) => complete(result)
    }
  }

  def deleteNotification(id: String)(implicit ctx: GlobalContext): Route = {
    val responseFuture = NotificationService.deleteNotification(id)(ctx.esClient)

    onComplete(responseFuture) {
      case Failure(_) => complete(StatusCodes.InternalServerError)
      case Success(result) => complete(result)
    }
  }
}
