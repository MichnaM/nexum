package nexum.http.controller

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives.{complete, onComplete}
import akka.http.scaladsl.server.Route
import nexum.GlobalContext
import nexum.domain.device.Device
import nexum.domain.device.State.unknown
import nexum.service.{DeviceDataPortionsService, DeviceService}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

object DeviceApiController {

  def getAllDevices(implicit ctx: GlobalContext): Route = {
    val responseFuture = DeviceService.getAllDevices(ctx.esClient)

    onComplete(responseFuture) {
      case Failure(_) => complete(StatusCodes.InternalServerError)
      case Success(result) => complete(result)
    }

  }

  def getDeviceByName(name: String)(implicit ctx: GlobalContext): Route = {
    val deviceFuture = DeviceService.getDeviceByDeviceID(name)(ctx.esClient).flatMap {
      case Some(device) =>
        val maxPeriod = device.config.getMaxPeriod
        DeviceDataPortionsService.getDataPortionsByDeviceID(name, maxPeriod)(ctx.esClient).map {
          case Some(portions) => Some(Device.insertDataPortions(device, Some(portions)))
          case None => Some(Device.insertDataPortions(device, Some(Nil)))
        }
      case None => Future.successful(None)
    }

    onComplete(deviceFuture) {
      case Failure(_) => complete(StatusCodes.InternalServerError)
      case Success(result) => result match {
          case Some(device) => complete(device)
          case None => complete(StatusCodes.NotFound)
        }
    }
  }

  def indexDevice(device: Device)(implicit ctx: GlobalContext): Route = {

    val deviceWithState = device.state match {
      case Some(_) => device
      case None => Device(
          deviceID   = device.deviceID,
          dmID       = device.dmID,
          config     = device.config,
          state      = Some(unknown),
          deviceData = device.deviceData
        )
    }
    val responseFuture  = DeviceService.indexDevice(deviceWithState)(ctx.esClient)

    onComplete(responseFuture) {
      case Failure(_) => complete(StatusCodes.InternalServerError)
      case Success(result) => result match {
          case Some(res) => complete(res)
          case None => complete(StatusCodes.InternalServerError)
        }
    }
  }

  def deleteDevice(name: String)(implicit ctx: GlobalContext): Route = {
    val responseFuture = DeviceService.deleteDevice(name)(ctx.esClient)

    onComplete(responseFuture) {
      case Failure(_) => complete(StatusCodes.InternalServerError)
      case Success(result) => result match {
          case Some(res) => complete(res)
          case None => complete(StatusCodes.InternalServerError)
        }
    }
  }

}
