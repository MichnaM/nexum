package nexum.http.controller

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives.{complete, onComplete}
import akka.http.scaladsl.server.Route
import nexum.GlobalContext
import nexum.domain.User
import nexum.service.UserService

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

object UserApiController {

  def getAllUsers(implicit ctx: GlobalContext): Route = {
    val responseFuture = UserService.getAllUsers(ctx.esClient).map {
      case Some(users) => Some(users.map(u => User(u.username)))
      case None => None
    }

    onComplete(responseFuture) {
      case Failure(_) => complete(StatusCodes.InternalServerError)
      case Success(result) => complete(result)
    }
  }

  def getUserByUsername(username: String)(implicit ctx: GlobalContext): Route = {
    val responseFuture = UserService.getUserByUsername(username)(ctx.esClient).map {
      case Some(user) => Some(User(user.username))
      case None => None
    }

    onComplete(responseFuture) {
      case Failure(_) => complete(StatusCodes.InternalServerError)
      case Success(result) => result match {
          case Some(user) => complete(user)
          case None => complete(StatusCodes.NotFound)
        }
    }
  }

  def indexUser(newUser: User)(implicit ctx: GlobalContext): Route = {
    val responseFuture = UserService.indexUser(newUser)(ctx.esClient)

    onComplete(responseFuture) {
      case Failure(_) => complete(StatusCodes.InternalServerError)
      case Success(result) => result match {
          case Some(result) => complete(result)
          case None => complete(StatusCodes.InternalServerError)
        }
    }
  }
}
