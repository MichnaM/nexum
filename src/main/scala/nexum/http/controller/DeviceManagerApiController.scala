package nexum.http.controller

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives.{complete, onComplete}
import akka.http.scaladsl.server.Route
import nexum.GlobalContext
import nexum.domain.devicemanager.DeviceManager
import nexum.service.DeviceManagerService

import scala.util.{Failure, Success}

object DeviceManagerApiController {

  def getAllDeviceManagers(implicit ctx: GlobalContext): Route = {
    val responseFuture = DeviceManagerService.getAllDeviceManagers(ctx.esClient)

    onComplete(responseFuture) {
      case Failure(_) => complete(StatusCodes.InternalServerError)
      case Success(result) => complete(result)
    }
  }

  def getDeviceManagerByName(name: String)(implicit ctx: GlobalContext): Route = {
    val responseFuture = DeviceManagerService.getDeviceManagerByID(name)(ctx.esClient)

    onComplete(responseFuture) {
      case Failure(_) => complete(StatusCodes.InternalServerError)
      case Success(result) => result match {
          case Some(dm) => complete(dm)
          case None => complete(StatusCodes.NotFound)
        }
    }
  }

  def indexDeviceManager(deviceManager: DeviceManager)(implicit ctx: GlobalContext): Route = {
    val responseFuture = DeviceManagerService.indexDeviceManager(deviceManager)(ctx.esClient)

    onComplete(responseFuture) {
      case Failure(_) => complete(StatusCodes.InternalServerError)
      case Success(result) => result match {
          case Some(dm) => complete(dm)
          case None => complete(StatusCodes.InternalServerError)
        }
    }
  }

}
