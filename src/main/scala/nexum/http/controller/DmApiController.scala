package nexum.http.controller

import akka.Done
import akka.http.scaladsl.model.{DateTime, StatusCodes}
import akka.http.scaladsl.server.Directives.{complete, onComplete}
import akka.http.scaladsl.server.Route
import com.sksamuel.elastic4s.requests.bulk.BulkRequest
import com.sksamuel.elastic4s.requests.indexes.IndexRequest
import io.circe.Json
import nexum.EsIndices.nexum_devicedataportions
import nexum.domain.device.{Command, Device, DoubleDataPortion, StringDataPortion}
import nexum.domain.devicemanager.DeviceDataPortion.DeviceDataPortionsIndexable
import nexum.domain.devicemanager.{DeviceDataPortion, DeviceDataRaw, DeviceManagerConfig, DeviceManagerControl}
import nexum.{GlobalContext, calculateObjectHash}
import nexum.lib.es.ElasticsearchClient
import nexum.service.{DeviceManagerService, DeviceService, DeviceStateService}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

object DmApiController {

  def getDeviceManagerConfig(dmID: String)(implicit ctx: GlobalContext): Route = {
    implicit val esClient: ElasticsearchClient = ctx.esClient

    val dmFuture      = DeviceManagerService.getDeviceManagerByID(dmID)
    val devicesFuture = DeviceService.getDevicesByDmID(dmID)

    val responseFuture =
      for {
        dm <- dmFuture
        devices <- devicesFuture
      } yield dm match {
        case Some(deviceManager) =>
          val deviceList = devices match {
            case Some(devs) => devs
            case None => Nil
          }
          Some(DeviceManagerConfig(deviceManager.interval, deviceList))
        case None => None
      }

    onComplete(responseFuture) {
      case Failure(_) => complete(StatusCodes.InternalServerError)
      case Success(result) => result match {
          case Some(dmc) => complete(dmc)
          case None => complete(StatusCodes.NotFound)
        }
    }
  }

  def getDeviceManagerControl(dmID: String)(implicit ctx: GlobalContext): Route = {
    implicit val esClient: ElasticsearchClient = ctx.esClient

    val commandsFuture: Future[Option[List[Command]]] =
      DeviceStateService.getNotExecutedStatesByDmID(dmID, ctx.config.fetchStatusTime)
        .map {
          case Some(states) =>
            DeviceStateService.confirmCommandExecuted(states.map(_._id.get))
            Some(states.map(_.deviceID))
          case None => None
        }.flatMap {
          case Some(ids) =>
            if (ids.nonEmpty) {
              DeviceService.getDevicesByIDList(ids).map {
                case Some(devices) =>
                  val res = devices.map(_.config.command)
                  Some(res)
                case None => None
              }
            } else {
              Future.successful(None)
            }
          case None => Future.successful(None)
        }

    val resultFuture =
      for {
        configHash <- calculateConfigHash(dmID)
        commandsOpt <- commandsFuture
      } yield commandsOpt match {
        case Some(commands) => DeviceManagerControl(configHash, commands)
        case None => DeviceManagerControl(configHash, Nil)
      }

    onComplete(resultFuture) {
      case Failure(_) => complete(StatusCodes.InternalServerError)
      case Success(result) => complete(result)
    }

  }

  def parseRawDmData(dmID: String, devicesDataRaw: List[DeviceDataRaw])(implicit ctx: GlobalContext): Route = {
    implicit val esClient: ElasticsearchClient = ctx.esClient

    val resultFuture = DeviceService.getDevicesByDmID(dmID).map {
      case Some(devices) => devices
      case None => Nil
    }.map { devices =>
      devicesDataRaw.map { deviceRaw => convertRawDataToDataPortion(deviceRaw, dmID, devices) }
    }.flatMap { deviceDataPortions =>
      if (deviceDataPortions.nonEmpty) {
        val indexRequests = deviceDataPortions.map { dataPortion =>
          IndexRequest(index = nexum_devicedataportions, source = Some(DeviceDataPortionsIndexable.json(dataPortion)))
        }
        esClient.execIndexBulk(BulkRequest(indexRequests))
      } else {
        Future.successful(Done)
      }

    }

    onComplete(resultFuture) {
      case Failure(_) => complete(StatusCodes.InternalServerError)
      case Success(_) => complete("Completed")
    }
  }

  private def convertRawDataToDataPortion(deviceRaw: DeviceDataRaw, dmID: String, devices: List[Device]): DeviceDataPortion = {
    val deviceConfig = devices.filter(_.deviceID == deviceRaw.deviceID).head.config
    val rawJsonMap   = flattenJson(_ + "_" + _)(deviceRaw.metricsRaw).asObject.get.toMap

    val doubleData = deviceConfig.doubleMetrics.map { metric =>
      val value = {
        try {
          Some(rawJsonMap(metric.name).asNumber.get.toDouble)
        } catch {
          case _: Throwable =>
            scribe.error(s"Error parsing metric ${metric.name}")
            None
        }
      }
      DoubleDataPortion(metric.name, value)
    }

    val stringData = deviceConfig.stringMetrics.map { metric =>
      val value = {
        try {
          rawJsonMap(metric.name).asString
        } catch {
          case _: Throwable =>
            scribe.error(s"Error parsing metric ${metric.name}")
            None
        }
      }
      StringDataPortion(metric.name, value)
    }
    DeviceDataPortion(
      deviceID = deviceRaw.deviceID,
      dmID        = dmID,
      timestamp   = DateTime.now,
      wifiData    = deviceRaw.wifiData,
      btData      = deviceRaw.btData,
      latencyData = deviceRaw.latencyData,
      doubleData  = doubleData,
      stringData  = stringData
    )
  }

  private def flattenJson(combineKeys: (String, String) => String)(value: Json): Json = {
    def flattenToFields(value: Json): Option[Iterable[(String, Json)]] =
      value.asObject.map(
        _.toIterable.flatMap {
          case (k, v) => flattenToFields(v) match {
              case None => List(k -> v)
              case Some(fields) => fields.map {
                  case (innerK, innerV) => combineKeys(k, innerK) -> innerV
                }
            }
        }
      )

    flattenToFields(value).fold(value)(Json.fromFields)
  }

  private def calculateConfigHash(dmID: String)(implicit esClient: ElasticsearchClient): Future[String] = {
    DeviceService.getDevicesByDmID(dmID).map {
      case Some(devices) =>
        val deviceConfigs = devices.map(_.config)
        calculateObjectHash(deviceConfigs)
      case None => calculateObjectHash(Nil)
    }
  }

}
