package nexum.http.route

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import nexum.GlobalContext
import nexum.domain.devicemanager.DeviceDataRaw
import nexum.http.NexumAuth
import nexum.http.controller.DmApiController

object DevicemanagerRoutes {
  def routes(implicit ctx: GlobalContext): Route = pathPrefix("dm") {
    authenticateBasicAsync("secure_devicemanager", NexumAuth.deviceManagerAuthenticator) { dmID =>
      configRoute(dmID) ~
        dataRoute(dmID) ~
        controlRoute(dmID)
    }
  }

  private def configRoute(dmID: String)(implicit ctx: GlobalContext) = pathPrefix("config") {
    pathEndOrSingleSlash {
      get {
        scribe.debug(s"API call: GET /dm/config")
        DmApiController.getDeviceManagerConfig(dmID)
      }
    }
  }

  private def controlRoute(dmId: String)(implicit ctx: GlobalContext) = pathPrefix("control") {
    pathEndOrSingleSlash {
      get {
        scribe.debug(s"API call: GET /dm/control/$dmId")
        DmApiController.getDeviceManagerControl(dmId)
      }
    }
  }

  private def dataRoute(dmId: String)(implicit ctx: GlobalContext) = pathPrefix("data") {
    pathEndOrSingleSlash {
      post {
        entity(as[List[DeviceDataRaw]]) { devicesDataRaw =>
          scribe.debug(s"API call: POST /dm/data - deviceIDs: ${devicesDataRaw.collect(_.deviceID).mkString(",")}")
          DmApiController.parseRawDmData(dmId, devicesDataRaw)
        }
      }
    }
  }

}
