package nexum.http.route

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import nexum.GlobalContext
import nexum.domain.devicemanager.DeviceManager
import nexum.domain.User
import nexum.domain.device.Device
import nexum.domain.notification.Notification
import nexum.http.NexumAuth
import nexum.http.controller.{DeviceApiController, DeviceManagerApiController, NotificationApiController, UserApiController}

object UserRoutes {
  def routes(implicit ctx: GlobalContext): Route = authenticateBasicAsync("secure_user", NexumAuth.userAuthenticator) { _ =>
    usersRoute ~
      devicemanagersRoute ~
      devicesRoute ~
      notificationsRoute
  }

  private def usersRoute(implicit ctx: GlobalContext): Route = pathPrefix("users") {
    pathEndOrSingleSlash {
      get {
        scribe.info(s"API call: GET /users")
        UserApiController.getAllUsers
      } ~
        post {
          entity(as[User]) { user =>
            scribe.info(s"API call: POST /users")
            UserApiController.indexUser(user)
          }
        }
    } ~
      pathPrefix(Segment) { username =>
        get {
          scribe.info(s"API call: GET /users/$username")
          UserApiController.getUserByUsername(username)
        }
      }
  }

  private def devicemanagersRoute(implicit ctx: GlobalContext): Route = pathPrefix("devicemanagers") {
    pathEndOrSingleSlash {
      get {
        scribe.info(s"API call: GET /devicemanagers")
        DeviceManagerApiController.getAllDeviceManagers
      } ~
        post {
          entity(as[DeviceManager]) { deviceManager =>
            scribe.info(s"API call: POST /devicemanagers: $deviceManager")
            DeviceManagerApiController.indexDeviceManager(deviceManager)
          }
        }
    } ~
      pathPrefix(Segment) { name =>
        scribe.info(s"API call: GET /devicemanagers/$name")
        get { DeviceManagerApiController.getDeviceManagerByName(name) }
      }
  }

  private def devicesRoute(implicit ctx: GlobalContext): Route = pathPrefix("devices") {
    pathEndOrSingleSlash {
      get {
        scribe.info(s"API call: GET /devices")
        DeviceApiController.getAllDevices
      } ~
        post {
          entity(as[Device]) { device =>
            scribe.info(s"API call: POST /devices: $device")
            DeviceApiController.indexDevice(device)
          }
        }
    } ~
      pathPrefix(Segment) { name =>
        get {
          scribe.info(s"API call: GET /device/$name")
          DeviceApiController.getDeviceByName(name)
        } ~
          delete { DeviceApiController.deleteDevice(name) }
      }
  }

  private def notificationsRoute(implicit ctx: GlobalContext): Route = pathPrefix("notifications") {
    pathEndOrSingleSlash {
      get {
        scribe.info(s"API call: GET /notifications")
        NotificationApiController.getNotifications
      } ~
        post {
          entity(as[Notification]) { notification =>
            scribe.info(s"API call: POST /notifications")
            NotificationApiController.indexNotification(notification)
          }
        }
    } ~
      pathPrefix(Segment) { id =>
        get {
          scribe.info(s"API call: GET /notifications/$id")
          NotificationApiController.getNotification(id)
        } ~
          delete {
            scribe.info(s"API call: DELETE /notifications/$id")
            NotificationApiController.deleteNotification(id)

          }
      }
  }
}
