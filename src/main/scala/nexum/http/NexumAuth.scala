package nexum.http

import akka.http.scaladsl.server.directives.Credentials
import nexum.GlobalContext
import nexum.lib.es.ElasticsearchClient
import nexum.service.{DeviceManagerService, UserService}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object NexumAuth {

  def userAuthenticator(credentials: Credentials)(implicit ctx: GlobalContext): Future[Option[String]] = {
    implicit val esClient: ElasticsearchClient = ctx.esClient
    credentials match {
      case p @ Credentials.Provided(username) =>
        UserService.getUserByUsername(username).map {
          case Some(user) => user.password match {
              case Some(password) => if (p.verify(password) && password.nonEmpty) Some(username) else None
              case None => None
            }
          case None => None
        }
      case _ => Future.successful(None)
    }
  }

  def deviceManagerAuthenticator(credentials: Credentials)(implicit ctx: GlobalContext): Future[Option[String]] = {
    implicit val esClient: ElasticsearchClient = ctx.esClient
    credentials match {
      case p @ Credentials.Provided(dmID) =>
        DeviceManagerService.getDeviceManagerByID(dmID).map {
          case Some(devicemanager) => if (p.verify(devicemanager.token)) Some(dmID) else None
          case None => None
        }
      case _ => Future.successful(None)
    }
  }

}
