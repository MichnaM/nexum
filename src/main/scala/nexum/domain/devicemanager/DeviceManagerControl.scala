package nexum.domain.devicemanager

import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import nexum.domain.device.Command

case class DeviceManagerControl(
  configHash: String,
  commands:   List[Command]
) extends FailFastCirceSupport

object DeviceManagerControl {
  implicit val deviceManagerControlDecoder: Decoder[DeviceManagerControl] = deriveDecoder[DeviceManagerControl]
  implicit val deviceManagerControlEncoder: Encoder[DeviceManagerControl] = deriveEncoder[DeviceManagerControl]

}
