package nexum.domain.devicemanager

import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe._
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import nexum.domain.device.DoubleDataPortion

case class DeviceDataRaw(
  deviceID:    String,
  wifiData:    DoubleDataPortion,
  btData:      DoubleDataPortion,
  latencyData: DoubleDataPortion,
  metricsRaw:  Json
) extends FailFastCirceSupport

object DeviceDataRaw {
  implicit val DeviceDataRawDecoder: Decoder[DeviceDataRaw] = deriveDecoder[DeviceDataRaw]
  implicit val DeviceDataRawEncoder: Encoder[DeviceDataRaw] = deriveEncoder[DeviceDataRaw]

}
