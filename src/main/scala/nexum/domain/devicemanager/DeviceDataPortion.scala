package nexum.domain.devicemanager

import akka.http.scaladsl.model.DateTime
import com.sksamuel.elastic4s.{Hit, HitReader, Indexable}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe._
import io.circe.generic.semiauto._
import io.circe.jawn.decode
import io.circe.syntax._
import nexum.domain.{dateTimeDecoder, dateTimeEncoder}
import nexum.domain.device.{DoubleDataPortion, StringDataPortion}

import scala.util.Try

case class DeviceDataPortion(
  deviceID:    String,
  dmID:        String,
  timestamp:   DateTime,
  wifiData:    DoubleDataPortion,
  btData:      DoubleDataPortion,
  latencyData: DoubleDataPortion,
  doubleData:  List[DoubleDataPortion],
  stringData:  List[StringDataPortion]
) extends FailFastCirceSupport

object DeviceDataPortion {

  implicit val deviceDataPortionsDecoder: Decoder[DeviceDataPortion] = deriveDecoder[DeviceDataPortion]
  implicit val deviceDataPortionsEncoder: Encoder[DeviceDataPortion] = deriveEncoder[DeviceDataPortion]

  implicit object DeviceDataPortionsHitReader extends HitReader[DeviceDataPortion] {
    override def read(hit: Hit): Try[DeviceDataPortion] = decode[DeviceDataPortion](hit.sourceAsString).toTry
  }

  implicit object DeviceDataPortionsIndexable extends Indexable[DeviceDataPortion] {
    override def json(t: DeviceDataPortion): String = t.asJson.dropNullValues.noSpaces
  }
}
