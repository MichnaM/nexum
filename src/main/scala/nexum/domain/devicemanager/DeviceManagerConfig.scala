package nexum.domain.devicemanager

import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import nexum.domain.device.Device

case class DeviceManagerConfig(
  interval: Double,
  devices:  List[Device]
) extends FailFastCirceSupport

object DeviceManagerConfig {
  implicit val deviceManagerConfigDecoder: Decoder[DeviceManagerConfig] = deriveDecoder[DeviceManagerConfig]
  implicit val deviceManagerConfigEncoder: Encoder[DeviceManagerConfig] = deriveEncoder[DeviceManagerConfig]

}
