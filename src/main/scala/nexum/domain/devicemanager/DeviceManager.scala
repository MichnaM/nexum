package nexum.domain.devicemanager

import com.sksamuel.elastic4s.{Hit, HitReader, Indexable}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe._
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.jawn.decode
import io.circe.syntax._

import scala.util.Try

case class DeviceManager(
  dmID:     String,
  token:    String,
  interval: Double
) extends FailFastCirceSupport

object DeviceManager {
  implicit val deviceManagerDecoder: Decoder[DeviceManager] = deriveDecoder[DeviceManager]
  implicit val deviceManagerEncoder: Encoder[DeviceManager] = deriveEncoder[DeviceManager].mapJsonObject { jsonObj =>
    jsonObj.filter { case (_, v) => !v.isNull }
  }

  implicit object DeviceManagerHitReader extends HitReader[DeviceManager] {
    override def read(hit: Hit): Try[DeviceManager] = {
      decode[DeviceManager](hit.sourceAsString).toTry
    }
  }

  implicit object DeviceManagerIndexable extends Indexable[DeviceManager] {
    override def json(t: DeviceManager): String = t.asJson.dropNullValues.noSpaces
  }
}
