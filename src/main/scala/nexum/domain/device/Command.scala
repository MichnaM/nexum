package nexum.domain.device

import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

case class Command(url: String, body: String) extends FailFastCirceSupport

object Command {
  implicit val commandDecoder: Decoder[Command] = deriveDecoder[Command]
  implicit val commandEncoder: Encoder[Command] = deriveEncoder[Command]
}
