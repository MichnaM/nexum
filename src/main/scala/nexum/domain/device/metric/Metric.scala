package nexum.domain.device.metric

trait Metric {
  def name:         String
  def period:       Double
  def passMinRatio: Double
}
