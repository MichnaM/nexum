package nexum.domain.device.metric

trait DoubleMetric extends Metric {
  def valueGrater: Double
  def valueLess:   Double
}
