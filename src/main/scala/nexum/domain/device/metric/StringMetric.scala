package nexum.domain.device.metric

trait StringMetric extends Metric {
  def valueIn: List[String]
}
