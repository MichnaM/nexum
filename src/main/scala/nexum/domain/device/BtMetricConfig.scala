package nexum.domain.device

import com.sksamuel.elastic4s.{Hit, HitReader, Indexable}
import io.circe._
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.jawn.decode
import io.circe.syntax._
import nexum.domain.device.metric.DoubleMetric

import scala.util.Try

case class BtMetricConfig(
  enabled:      Boolean,
  name:         String = "",
  btName:       String = "",
  period:       Double = 10,
  passMinRatio: Double = 0.5,
  valueGrater:  Double = 0,
  valueLess:    Double = 0
) extends DoubleMetric

object BtMetricConfig {
  implicit val btMetricConfigDecoder: Decoder[BtMetricConfig] = deriveDecoder[BtMetricConfig]
  implicit val btMetricConfigEncoder: Encoder[BtMetricConfig] = deriveEncoder[BtMetricConfig].mapJsonObject { jsonObj =>
    jsonObj.filter { case (_, v) => !v.isNull }
  }

  implicit object BtMetricConfigHitReader extends HitReader[BtMetricConfig] {
    override def read(hit: Hit): Try[BtMetricConfig] = {
      decode[BtMetricConfig](hit.sourceAsString).toTry
    }
  }

  implicit object BtMetricConfigIndexable extends Indexable[BtMetricConfig] {
    override def json(t: BtMetricConfig): String = t.asJson.dropNullValues.noSpaces
  }
}
