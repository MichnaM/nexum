package nexum.domain.device

import com.sksamuel.elastic4s.{Hit, HitReader, Indexable}
import io.circe._
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.jawn.decode
import io.circe.syntax._
import nexum.domain.device.metric.DoubleMetric

import scala.util.Try

case class DoubleMetricConfig(
  name:         String,
  period:       Double,
  passMinRatio: Double = 0.5,
  valueGrater:  Double,
  valueLess:    Double
) extends DoubleMetric

object DoubleMetricConfig {
  implicit val doubleMetricConfig:  Decoder[DoubleMetricConfig] = deriveDecoder[DoubleMetricConfig]
  implicit val doubleMetricEncoder: Encoder[DoubleMetricConfig] = deriveEncoder[DoubleMetricConfig].mapJsonObject { jsonObj =>
    jsonObj.filter { case (_, v) => !v.isNull }
  }

  implicit object DoubleMetricConfigHitReader extends HitReader[DoubleMetricConfig] {
    override def read(hit: Hit): Try[DoubleMetricConfig] = {
      decode[DoubleMetricConfig](hit.sourceAsString).toTry
    }
  }

  implicit object DoubleMetricConfigIndexable extends Indexable[DoubleMetricConfig] {
    override def json(t: DoubleMetricConfig): String = t.asJson.dropNullValues.noSpaces
  }
}
