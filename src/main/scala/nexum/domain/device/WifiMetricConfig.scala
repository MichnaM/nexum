package nexum.domain.device

import com.sksamuel.elastic4s.{Hit, HitReader, Indexable}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps
import io.circe.{Decoder, Encoder}
import nexum.domain.device.metric.DoubleMetric

import scala.util.Try

case class WifiMetricConfig(
  enabled:      Boolean = false,
  name:         String  = "",
  ssid:         String  = "",
  period:       Double  = 10,
  passMinRatio: Double  = 0.5,
  valueGrater:  Double  = 0,
  valueLess:    Double  = 0
) extends DoubleMetric

object WifiMetricConfig {
  implicit val wifiMetricConfigDecoder: Decoder[WifiMetricConfig] = deriveDecoder[WifiMetricConfig]
  implicit val wifiMetricConfigEncoder: Encoder[WifiMetricConfig] = deriveEncoder[WifiMetricConfig].mapJsonObject { jsonObj =>
    jsonObj.filter { case (_, v) => !v.isNull }
  }

  implicit object WifiMetricConfigHitReader extends HitReader[WifiMetricConfig] {
    override def read(hit: Hit): Try[WifiMetricConfig] = {
      decode[WifiMetricConfig](hit.sourceAsString).toTry
    }
  }

  implicit object WifiMetricConfigIndexable extends Indexable[WifiMetricConfig] {
    override def json(t: WifiMetricConfig): String = t.asJson.dropNullValues.noSpaces
  }
}
