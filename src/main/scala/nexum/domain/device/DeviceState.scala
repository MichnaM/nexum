package nexum.domain.device

import akka.http.scaladsl.model.DateTime
import com.sksamuel.elastic4s.{Hit, HitReader, Indexable}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe._
import io.circe.generic.semiauto._
import io.circe.jawn.decode
import io.circe.syntax._
import nexum.domain.device.State.State
import nexum.domain.{dateTimeDecoder, dateTimeEncoder}

import scala.util.Try

case class DeviceState(
  _id:             Option[String] = None,
  dmId:            String,
  deviceID:        String,
  state:           State,
  timestamp:       DateTime,
  notified:        Boolean        = false,
  commandExecuted: Boolean        = false
) extends FailFastCirceSupport

object DeviceState {

  def fromDeviceData(device: Device, state: State, timestamp: DateTime): DeviceState =
    DeviceState(dmId = device.dmID, deviceID = device.deviceID, state = state, timestamp = timestamp)

  def insertID(id: String, ds: DeviceState): DeviceState =
    DeviceState(
      _id             = Some(id),
      dmId            = ds.dmId,
      deviceID        = ds.deviceID,
      state           = ds.state,
      timestamp       = ds.timestamp,
      notified        = ds.notified,
      commandExecuted = ds.commandExecuted
    )

  implicit val deviceStateDecoder: Decoder[DeviceState] = deriveDecoder[DeviceState]
  implicit val deviceStateEncoder: Encoder[DeviceState] = deriveEncoder[DeviceState].mapJsonObject { jsonObj =>
    jsonObj.filter { case (_, v) => !v.isNull }
  }

  implicit object DeviceHitReader extends HitReader[DeviceState] {
    override def read(hit: Hit): Try[DeviceState] = {
      decode[DeviceState](hit.sourceAsString).toTry.map { DeviceState.insertID(hit.id, _) }
    }
  }

  implicit object DeviceIndexable extends Indexable[DeviceState] {
    override def json(t: DeviceState): String = t.asJson.dropNullValues.noSpaces
  }
}
