package nexum.domain.device

import com.sksamuel.elastic4s.{Hit, HitReader, Indexable}
import io.circe._
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.jawn.decode
import io.circe.syntax._
import nexum.domain.device.metric.StringMetric

import scala.util.Try

case class StringMetricConfig(
  name:         String,
  period:       Double,
  passMinRatio: Double = 0.5,
  valueIn:      List[String]
) extends StringMetric

object StringMetricConfig {
  implicit val stringMetricConfig:  Decoder[StringMetricConfig] = deriveDecoder[StringMetricConfig]
  implicit val stringMetricEncoder: Encoder[StringMetricConfig] = deriveEncoder[StringMetricConfig].mapJsonObject { jsonObj =>
    jsonObj.filter { case (_, v) => !v.isNull }
  }

  implicit object StringMetricConfigHitReader extends HitReader[StringMetricConfig] {
    override def read(hit: Hit): Try[StringMetricConfig] = {
      decode[StringMetricConfig](hit.sourceAsString).toTry
    }
  }

  implicit object StringMetricConfigIndexable extends Indexable[StringMetricConfig] {
    override def json(t: StringMetricConfig): String = t.asJson.dropNullValues.noSpaces
  }
}
