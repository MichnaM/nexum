package nexum.domain.device

import io.circe.{Decoder, Encoder}

object State extends Enumeration {
  type State = String
  val green   = "green"
  val red     = "red"
  val unknown = "unknown"

  implicit val deviceStatusDecoder: Decoder[State.Value] = Decoder.decodeEnumeration(State)
  implicit val deviceStatusEncoder: Encoder[State.Value] = Encoder.encodeEnumeration(State)
}
