package nexum.domain.device

import com.sksamuel.elastic4s.{Hit, HitReader, Indexable}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe._
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.jawn.decode
import io.circe.syntax._
import nexum.domain.device.State.{State, unknown}
import nexum.domain.devicemanager.DeviceDataPortion

import scala.util.Try

case class Device(
  deviceID:   String,
  dmID:       String,
  config:     DeviceConfig,
  state:      Option[State]                   = Some(unknown),
  deviceData: Option[List[DeviceDataPortion]] = None
) extends FailFastCirceSupport

object Device {

  def insertDataPortions(device: Device, deviceData: Option[List[DeviceDataPortion]]): Device =
    Device(
      deviceID   = device.deviceID,
      dmID       = device.dmID,
      config     = device.config,
      deviceData = deviceData
    )

  implicit val deviceDecoder: Decoder[Device] = deriveDecoder[Device]
  implicit val deviceEncoder: Encoder[Device] = deriveEncoder[Device].mapJsonObject { jsonObj =>
    jsonObj.filter { case (_, v) => !v.isNull }
  }

  implicit object DeviceHitReader extends HitReader[Device] {
    override def read(hit: Hit): Try[Device] = {
      decode[Device](hit.sourceAsString).toTry
    }
  }

  implicit object DeviceIndexable extends Indexable[Device] {
    override def json(t: Device): String = t.asJson.dropNullValues.noSpaces
  }
}
