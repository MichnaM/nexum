package nexum.domain.device

import com.sksamuel.elastic4s.{Hit, HitReader, Indexable}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe._
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps

import scala.util.Try

trait DataPortion {
  def name: String
}

case class StringDataPortion(name: String, value: Option[String]) extends DataPortion with FailFastCirceSupport

object StringDataPortion {
  implicit val stringDataPortionDecoder: Decoder[StringDataPortion] = deriveDecoder[StringDataPortion]
  implicit val stringDataPortionEncoder: Encoder[StringDataPortion] = deriveEncoder[StringDataPortion].mapJsonObject { jsonObj =>
    jsonObj.filter { case (_, v) => !v.isNull }
  }

  implicit object StringDataPortionHitReader extends HitReader[StringDataPortion] {
    override def read(hit: Hit): Try[StringDataPortion] = {
      decode[StringDataPortion](hit.sourceAsString).toTry
    }
  }

  implicit object StringDataPortionIndexable extends Indexable[StringDataPortion] {
    override def json(t: StringDataPortion): String = t.asJson.dropNullValues.noSpaces
  }
}

case class DoubleDataPortion(name: String, value: Option[Double]) extends DataPortion with FailFastCirceSupport

object DoubleDataPortion {
  implicit val doubleDataPortionDecoder: Decoder[DoubleDataPortion] = deriveDecoder[DoubleDataPortion]
  implicit val doubleDataPortionEncoder: Encoder[DoubleDataPortion] = deriveEncoder[DoubleDataPortion].mapJsonObject { jsonObj =>
    jsonObj.filter { case (_, v) => !v.isNull }
  }

  implicit object DoubleDataPortionHitReader extends HitReader[DoubleDataPortion] {
    override def read(hit: Hit): Try[DoubleDataPortion] = {
      decode[DoubleDataPortion](hit.sourceAsString).toTry
    }
  }

  implicit object DoubleDataPortionIndexable extends Indexable[DoubleDataPortion] {
    override def json(t: DoubleDataPortion): String = t.asJson.dropNullValues.noSpaces
  }
}
