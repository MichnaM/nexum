package nexum.domain.device

import com.sksamuel.elastic4s.{Hit, HitReader, Indexable}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps
import io.circe.{Decoder, Encoder}
import nexum.domain.device.metric.DoubleMetric

import scala.util.Try

case class LatencyMetricConfig(
  enabled:      Boolean,
  name:         String = "",
  url:          String = "",
  period:       Double = 10,
  passMinRatio: Double = 0.5,
  valueGrater:  Double = 0,
  valueLess:    Double = 0
) extends DoubleMetric

object LatencyMetricConfig {
  implicit val latencyMetricConfigDecoder: Decoder[LatencyMetricConfig] = deriveDecoder[LatencyMetricConfig]
  implicit val latencyMetricConfigEncoder: Encoder[LatencyMetricConfig] =
    deriveEncoder[LatencyMetricConfig].mapJsonObject { jsonObj =>
      jsonObj.filter { case (_, v) => !v.isNull }
    }

  implicit object LatencyMetricConfigHitReader extends HitReader[LatencyMetricConfig] {
    override def read(hit: Hit): Try[LatencyMetricConfig] = {
      decode[LatencyMetricConfig](hit.sourceAsString).toTry
    }
  }

  implicit object LatencyMetricConfigIndexable extends Indexable[LatencyMetricConfig] {
    override def json(t: LatencyMetricConfig): String = t.asJson.dropNullValues.noSpaces
  }
}
