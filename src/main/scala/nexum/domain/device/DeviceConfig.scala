package nexum.domain.device

import com.sksamuel.elastic4s.{Hit, HitReader, Indexable}
import io.circe._
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.jawn.decode
import io.circe.syntax._

import scala.util.Try

case class DeviceConfig(
  command:       Command,
  url:           String,
  wifiMetric:    WifiMetricConfig,
  btMetric:      BtMetricConfig,
  latencyMetric: LatencyMetricConfig,
  doubleMetrics: List[DoubleMetricConfig],
  stringMetrics: List[StringMetricConfig]
) {
  def getMaxPeriod: Double = {
    val doubleMetricsPeriods = doubleMetrics.map(_.period)
    val stringMetricsPeriods = stringMetrics.map(_.period)
    val periods              = wifiMetric.period :: btMetric.period :: latencyMetric.period :: doubleMetricsPeriods ::: stringMetricsPeriods
    periods.max
  }
}

object DeviceConfig {
  implicit val deviceConfigDecoder: Decoder[DeviceConfig] = deriveDecoder[DeviceConfig]
  implicit val deviceConfigEncoder: Encoder[DeviceConfig] = deriveEncoder[DeviceConfig].mapJsonObject { jsonObj =>
    jsonObj.filter { case (_, v) => !v.isNull }
  }

  implicit object DeviceConfigHitReader extends HitReader[DeviceConfig] {
    override def read(hit: Hit): Try[DeviceConfig] = {
      decode[DeviceConfig](hit.sourceAsString).toTry
    }
  }

  implicit object DeviceConfigIndexable extends Indexable[DeviceConfig] {
    override def json(device: DeviceConfig): String = device.asJson.dropNullValues.noSpaces
  }
}
