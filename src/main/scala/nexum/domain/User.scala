package nexum.domain

import com.sksamuel.elastic4s.{Hit, HitReader, Indexable}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe._
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.jawn.decode
import io.circe.syntax._

import scala.util.Try

case class User(
  username: String,
  password: Option[String] = None
) extends FailFastCirceSupport

object User {
  implicit val userDecoder: Decoder[User] = deriveDecoder[User]
  implicit val userEncoder: Encoder[User] = deriveEncoder[User].mapJsonObject { jsonObj =>
    jsonObj.filter { case (_, v) => !v.isNull }
  }

  implicit object UserHitReader extends HitReader[User] {
    override def read(hit: Hit): Try[User] = {
      decode[User](hit.sourceAsString).toTry
    }
  }

  implicit object UserIndexable extends Indexable[User] {
    override def json(user: User): String = user.asJson.dropNullValues.noSpaces
  }
}
