package nexum

import akka.http.scaladsl.model.DateTime
import io.circe._
import io.circe.syntax._

package object domain {
  implicit val dateTimeEncoder: Encoder[DateTime] = Encoder.instance(a => a.toIsoDateTimeString.asJson)
  implicit val dateTimeDecoder: Decoder[DateTime] = Decoder.instance(a =>
    a.as[String].map(isoDateTimeString =>
      DateTime.fromIsoDateTimeString(isoDateTimeString).getOrElse(DateTime(0))
    )
  )
}
