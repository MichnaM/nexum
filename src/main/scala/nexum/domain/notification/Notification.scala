package nexum.domain.notification

import com.sksamuel.elastic4s.{Hit, HitReader, Indexable}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps

import scala.util.Try

case class Notification(
  id:        String,
  receivers: List[String],
  deviceIDs: List[String]
) extends FailFastCirceSupport

object Notification {
  implicit val notificationDecoder: Decoder[Notification] = deriveDecoder[Notification]
  implicit val notificationEncoder: Encoder[Notification] = deriveEncoder[Notification]

  implicit object NotificationHitReader extends HitReader[Notification] {
    override def read(hit: Hit): Try[Notification] = {
      decode[Notification](hit.sourceAsString).toTry
    }
  }

  implicit object NotificationIndexable extends Indexable[Notification] {
    override def json(t: Notification): String = t.asJson.dropNullValues.noSpaces
  }
}
