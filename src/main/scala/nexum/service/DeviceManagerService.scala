package nexum.service

import com.sksamuel.elastic4s.requests.common.RefreshPolicy
import com.sksamuel.elastic4s.requests.get.GetRequest
import com.sksamuel.elastic4s.requests.indexes.IndexRequest
import com.sksamuel.elastic4s.requests.searches.SearchRequest
import nexum.EsIndices.nexum_devicemanagers
import nexum.domain.devicemanager.DeviceManager
import nexum.lib.es.{ElasticResult, ElasticsearchClient}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object DeviceManagerService {
  def getDeviceManagerByID(dmID: String)(implicit esClient: ElasticsearchClient): Future[Option[DeviceManager]] = {
    try {
      esClient.execGet(GetRequest(nexum_devicemanagers, dmID)).map { response =>
        if (response.found) {
          Some(response.to[DeviceManager])
        } else {
          scribe.debug(s"getDeviceManagerByID - Not found dm $dmID")
          None
        }
      }
    } catch {
      case e: Throwable =>
        scribe.debug(s"Error getting devicemanager $dmID - ${e.getMessage}")
        Future.successful(None)
    }
  }

  def getAllDeviceManagers(implicit esClient: ElasticsearchClient): Future[Option[List[DeviceManager]]] = {
    try {
      esClient.execSearch(SearchRequest(nexum_devicemanagers).matchAllQuery).map { response =>
        val users = response.to[DeviceManager].toList
        Some(users)
      }
    } catch {
      case e: Throwable =>
        scribe.debug(s"Error getting list of devicemanagers - ${e.getMessage}")
        Future.successful(None)
    }
  }

  def indexDeviceManager(deviceManager: DeviceManager)(implicit esClient: ElasticsearchClient): Future[Option[ElasticResult]] = {
    val indexRequest = IndexRequest(
      index   = nexum_devicemanagers,
      id      = Some(deviceManager.dmID),
      refresh = Some(RefreshPolicy.IMMEDIATE)
    ).doc(deviceManager)

    try {
      esClient.execIndex(indexRequest).map { response =>
        scribe.debug(s"indexDeviceManager - trying to index devicemanager ${deviceManager.dmID}: ${response.result}")
        Some(ElasticResult(response.result))
      }
    } catch {
      case e: Throwable =>
        scribe.debug(s"Error adding or updating devicemanager to ES - ${e.getMessage}")
        Future.successful(None)
    }
  }
}
