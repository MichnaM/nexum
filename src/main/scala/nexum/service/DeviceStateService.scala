package nexum.service

import com.sksamuel.elastic4s.requests.bulk.{BulkRequest, BulkResponse}
import com.sksamuel.elastic4s.requests.common.RefreshPolicy.Immediate
import com.sksamuel.elastic4s.requests.indexes.IndexRequest
import com.sksamuel.elastic4s.requests.searches.SearchRequest
import com.sksamuel.elastic4s.requests.searches.queries.RangeQuery
import com.sksamuel.elastic4s.requests.searches.queries.compound.BoolQuery
import com.sksamuel.elastic4s.requests.searches.queries.matches.MatchQuery
import com.sksamuel.elastic4s.requests.update.UpdateRequest
import nexum.EsIndices.nexum_devicestates
import nexum.domain.device.DeviceState
import nexum.lib.es.{ElasticResult, ElasticsearchClient}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object DeviceStateService {

  def getLatestStateByDeviceID(deviceID: String)(implicit esClient: ElasticsearchClient): Future[Option[DeviceState]] = {
    try {
      esClient.execSearch(
        SearchRequest(nexum_devicestates)
          .matchQuery("deviceID", deviceID)
          .sortByFieldDesc("timestamp")
          .limit(1)
      ).map { _.to[DeviceState].headOption }
    } catch {
      case e: Throwable =>
        scribe.info(s"Error getting latest DeviceState for device: $deviceID - ${e.getMessage}")
        Future.successful(None)
    }
  }

  def getNotExecutedStatesByDmID(dmID: String, age: Double)(implicit
    esClient:                          ElasticsearchClient
  ): Future[Option[List[DeviceState]]] = {
    try {
      esClient.execSearch(
        SearchRequest(nexum_devicestates).query(BoolQuery(
          must    = MatchQuery("dmID", dmID) :: MatchQuery("commandExecuted", false) :: Nil,
          filters = RangeQuery("timestamp", gte = Some(s"now-${age.toInt}s/s")) :: Nil
        ))
      ).map { res => Some(res.to[DeviceState].toList) }
    } catch {
      case e: Throwable =>
        scribe.info(s"Error getting latest DeviceStates for dm: $dmID - ${e.getMessage}")
        Future.successful(None)
    }
  }

  def confirmCommandExecuted(ids: List[String])(implicit esClient: ElasticsearchClient): Future[Option[BulkResponse]] = {
    val updateRequests = ids.map(id =>
      UpdateRequest(
        index          = nexum_devicestates,
        id             = id,
        documentFields = Map("commandExecuted" -> true)
      )
    )

    try {
      esClient.execIndexBulk(BulkRequest(updateRequests).refresh(Immediate)).map(Some(_))
    } catch {
      case e: Throwable =>
        scribe.info(s"Error updating commandExecuted for DeviceStates, ids: $ids - ${e.getMessage}")
        Future.successful(None)
    }
  }

  def indexDeviceState(deviceState: DeviceState)(implicit esClient: ElasticsearchClient): Future[Option[ElasticResult]] = {
    val indexRequest = IndexRequest(index = nexum_devicestates).doc(deviceState)

    try {
      esClient.execIndex(indexRequest).map { response =>
        Some(ElasticResult(response.result))
      }
    } catch {
      case e: Throwable =>
        scribe.info(s"Error indexing DeviceState to ES")
        Future.successful(None)
    }
  }

  def getNotNotified(age: Double)(implicit esClient: ElasticsearchClient): Future[Option[List[DeviceState]]] = {
    try {
      esClient.execSearch(
        SearchRequest(nexum_devicestates).query(BoolQuery(
          must    = MatchQuery("notified", false) :: Nil,
          filters = RangeQuery("timestamp", gte = Some(s"now-${age.toInt}s/s")) :: Nil
        ))
      ).map { res => Some(res.to[DeviceState].toList) }
    } catch {
      case e: Throwable =>
        scribe.info(s"Error getting not notified DeviceStates - ${e.getMessage}")
        Future.successful(None)
    }
  }

  def confirmNotified(ids: List[String])(implicit esClient: ElasticsearchClient): Future[Option[BulkResponse]] = {
    val updateRequests = ids.map(id =>
      UpdateRequest(
        index          = nexum_devicestates,
        id             = id,
        documentFields = Map("notified" -> true)
      )
    )
    try {
      esClient.execIndexBulk(BulkRequest(updateRequests).refresh(Immediate)).map(Some(_))
    } catch {
      case e: Throwable =>
        scribe.info(s"Error updating commandExecuted for DeviceStates, ids: $ids - ${e.getMessage}")
        Future.successful(None)
    }

  }

}
