package nexum.service

import com.sksamuel.elastic4s.requests.common.RefreshPolicy
import com.sksamuel.elastic4s.requests.delete.DeleteByIdRequest
import com.sksamuel.elastic4s.requests.get.GetRequest
import com.sksamuel.elastic4s.requests.indexes.IndexRequest
import com.sksamuel.elastic4s.requests.searches.SearchRequest
import nexum.EsIndices.nexum_devices
import nexum.domain.device.Device
import nexum.lib.es.{ElasticResult, ElasticsearchClient}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object DeviceService {
  def getDeviceByDeviceID(deviceID: String)(implicit esClient: ElasticsearchClient): Future[Option[Device]] = {
    try {
      esClient.execGet(GetRequest(nexum_devices, deviceID)).map { response =>
        if (response.found) {
          Some(response.to[Device])
        } else {
          None
        }
      }
    } catch {
      case e: Throwable =>
        scribe.info(s"Error getting device $deviceID - ${e.getMessage}")
        Future.successful(None)
    }
  }

  def getAllDevices(implicit esClient: ElasticsearchClient): Future[Option[List[Device]]] = {
    try {
      esClient.execSearch(SearchRequest(nexum_devices).matchAllQuery).map { response =>
        val devices = response.to[Device].toList
        Some(devices)
      }
    } catch {
      case e: Throwable =>
        scribe.info(s"Error getting list of devices - ${e.getMessage}")
        Future.successful(None)
    }
  }

  def indexDevice(device: Device)(implicit esClient: ElasticsearchClient): Future[Option[ElasticResult]] = {
    val indexRequest = IndexRequest(
      index   = nexum_devices,
      id      = Some(device.deviceID),
      refresh = Some(RefreshPolicy.IMMEDIATE)
    ).doc(device)

    try {
      esClient.execIndex(indexRequest).map { response =>
        Some(ElasticResult(response.result))
      }
    } catch {
      case e: Throwable =>
        scribe.info(s"Error adding or updating device to ES - ${e.getMessage}")
        Future.successful(None)
    }
  }

  def deleteDevice(deviceID: String)(implicit esClient: ElasticsearchClient): Future[Option[ElasticResult]] = {
    val deleteRequest = DeleteByIdRequest(nexum_devices, deviceID)

    try {
      esClient.execDelete(deleteRequest).map { response =>
        Some(ElasticResult(response.result))
      }
    } catch {
      case e: Throwable =>
        scribe.info(s"Error deleting device from ES - ${e.getMessage}")
        Future.successful(None)
    }

  }

  def getDevicesByDmID(dmID: String)(implicit esClient: ElasticsearchClient): Future[Option[List[Device]]] = {
    val searchRequest = SearchRequest(nexum_devices).matchQuery("dmID", dmID)

    try {
      esClient.execSearch(searchRequest).map { response =>
        val devices = response.to[Device].toList
        Some(devices)
      }
    } catch {
      case e: Throwable =>
        scribe.info(s"Error getting list of devices by dmID - ${e.getMessage}")
        Future.successful(None)
    }

  }

  def getDevicesByIDList(ids: List[String])(implicit esClient: ElasticsearchClient): Future[Option[List[Device]]] = {
    val getRequests = ids.map(id => GetRequest(nexum_devices, id))

    try {
      esClient.execMultiGet(getRequests).map { response =>
        val devices = response.to[Device].toList
        Some(devices)
      }
    } catch {
      case e: Throwable =>
        scribe.info(s"Error getting list of devices by ids - ${e.getMessage}")
        Future.successful(None)
    }
  }

}
