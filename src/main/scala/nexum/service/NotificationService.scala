package nexum.service

import com.sksamuel.elastic4s.requests.delete.DeleteByIdRequest
import com.sksamuel.elastic4s.requests.get.GetRequest
import com.sksamuel.elastic4s.requests.indexes.IndexRequest
import com.sksamuel.elastic4s.requests.searches.SearchRequest
import nexum.EsIndices.nexum_notifications
import nexum.domain.notification.Notification
import nexum.lib.es.{ElasticResult, ElasticsearchClient}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object NotificationService {

  def getNotification(id: String)(implicit esClient: ElasticsearchClient): Future[Option[Notification]] = {
    try {
      esClient.execGet(GetRequest(nexum_notifications, id)).map { response =>
        if (response.found) {
          Some(response.to[Notification])
        } else {
          None
        }
      }
    } catch {
      case e: Throwable =>
        scribe.info(s"Error getting notification config: $id - ${e.getMessage}")
        Future.successful(None)
    }
  }

  def getNotifications(implicit esClient: ElasticsearchClient): Future[Option[List[Notification]]] = {
    try {
      esClient.execSearch(SearchRequest(nexum_notifications).matchAllQuery).map { response =>
        val notifications = response.to[Notification].toList
        Some(notifications)
      }
    } catch {
      case e: Throwable =>
        scribe.info(s"Error getting notification config list - ${e.getMessage}")
        Future.successful(None)
    }
  }

  def indexNotification(notification: Notification)(implicit
    esClient:                         ElasticsearchClient
  ): Future[Option[ElasticResult]] = {
    val indexRequest = IndexRequest(
      index = nexum_notifications,
      id    = Some(notification.id)
    ).doc(notification)

    try {
      esClient.execIndex(indexRequest).map { response =>
        Some(ElasticResult(response.result))
      }
    } catch {
      case e: Throwable =>
        scribe.info(s"Error adding or updating device to ES - ${e.getMessage}")
        Future.successful(None)
    }
  }

  def deleteNotification(id: String)(implicit esClient: ElasticsearchClient): Future[Option[ElasticResult]] = {
    val deleteRequest = DeleteByIdRequest(nexum_notifications, id)

    try {
      esClient.execDelete(deleteRequest).map { response =>
        Some(ElasticResult(response.result))
      }
    } catch {
      case e: Throwable =>
        scribe.info(s"Error deleting device from ES - ${e.getMessage}")
        Future.successful(None)
    }
  }

}
