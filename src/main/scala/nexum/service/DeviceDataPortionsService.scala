package nexum.service

import com.sksamuel.elastic4s.requests.indexes.IndexRequest
import com.sksamuel.elastic4s.requests.searches.SearchRequest
import com.sksamuel.elastic4s.requests.searches.queries.RangeQuery
import com.sksamuel.elastic4s.requests.searches.queries.compound.BoolQuery
import com.sksamuel.elastic4s.requests.searches.queries.matches.MatchQuery
import nexum.EsIndices.nexum_devicedataportions
import nexum.domain.devicemanager.DeviceDataPortion
import nexum.lib.es.{ElasticResult, ElasticsearchClient}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object DeviceDataPortionsService {

  def getDataPortionsByDeviceID(id: String, age: Double)(implicit
    esClient:                       ElasticsearchClient
  ): Future[Option[List[DeviceDataPortion]]] = {
    try {
      esClient.execSearch(SearchRequest(nexum_devicedataportions).query(BoolQuery(
        must    = MatchQuery("deviceID", id) :: Nil,
        filters = RangeQuery("timestamp", gte = Some(s"now-${age.toInt}s/s")) :: Nil
      ))).map { response =>
        val dataPortions = response.to[DeviceDataPortion].toList
        Some(dataPortions)
      }
    } catch {
      case e: Throwable =>
        scribe.info(s"Error getting list of devicedataportions for device: $id - ${e.getMessage}")
        Future.successful(None)
    }
  }

  def indexDeviceDataPortions(deviceDataPortions: DeviceDataPortion)(implicit
    esClient:                                     ElasticsearchClient
  ): Future[Option[ElasticResult]] = {

    val indexRequest = IndexRequest(index = nexum_devicedataportions).doc(deviceDataPortions)

    try {
      esClient.execIndex(indexRequest).map { response =>
        Some(ElasticResult(response.result))
      }
    } catch {
      case e: Throwable =>
        scribe.info(s"Error adding DeviceDataPortions to ES - ${e.getMessage}")
        Future.successful(None)
    }
  }

}
