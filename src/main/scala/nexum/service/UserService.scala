package nexum.service

import com.sksamuel.elastic4s.circe.hitReaderWithCirce
import com.sksamuel.elastic4s.requests.common.RefreshPolicy
import com.sksamuel.elastic4s.requests.get.GetRequest
import com.sksamuel.elastic4s.requests.indexes.IndexRequest
import com.sksamuel.elastic4s.requests.searches.SearchRequest
import nexum.EsIndices.nexum_users
import nexum.domain.User
import nexum.lib.es.{ElasticResult, ElasticsearchClient}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object UserService {

  def getUserByUsername(username: String)(implicit esClient: ElasticsearchClient): Future[Option[User]] = {
    try {
      esClient.execGet(GetRequest(nexum_users, username)).map { response =>
        if (response.found) {
          Some(response.to[User])
        } else {
          None
        }
      }
    } catch {
      case e: Throwable =>
        scribe.debug(s"Error getting user $username - ${e.getMessage}")
        Future.successful(None)
    }
  }

  def getAllUsers(implicit esClient: ElasticsearchClient): Future[Option[List[User]]] = {
    try {
      esClient.execSearch(SearchRequest(nexum_users).matchAllQuery).map { response =>
        val users = response.to[User].toList
        Some(users)
      }
    } catch {
      case e: Throwable =>
        scribe.debug(s"Error getting list of users - ${e.getMessage}")
        Future.successful(None)
    }
  }

  def indexUser(newUser: User)(implicit esClient: ElasticsearchClient): Future[Option[ElasticResult]] = {
    val indexRequest = IndexRequest(
      index   = nexum_users,
      id      = Some(newUser.username),
      refresh = Some(RefreshPolicy.IMMEDIATE)
    ).doc(newUser)
    try {
      esClient.execIndex(indexRequest).map { response =>
        Some(ElasticResult(response.result))
      }
    } catch {
      case e: Throwable =>
        scribe.debug(s"Error adding new user to ES - ${e.getMessage}")
        Future.successful(None)
    }
  }

}
