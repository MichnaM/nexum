import akka.http.scaladsl.Http
import akka.http.scaladsl.model.headers.{Authorization, BasicHttpCredentials}
import akka.http.scaladsl.model.{ContentTypes, HttpMethods, HttpRequest, StatusCodes, headers}
import nexum.NexumApp.actorSystem

import java.math.BigInteger
import java.security.MessageDigest
import scala.annotation.tailrec
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success}

package object nexum {
  object EsIndices extends Enumeration {
    type EsIndices = String
    val nexum_users              = "nexum_users"
    val nexum_devicemanagers     = "nexum_devicemanagers"
    val nexum_devicedataportions = "nexum_devicedataportions"
    val nexum_devices            = "nexum_devices"
    val nexum_devicestates       = "nexum_devicestates"
    val nexum_notifications      = "nexum_notifications"
  }

  def calculateObjectHash(obj: Any): String = {
    val digest = MessageDigest.getInstance("SHA-256").digest(
      obj.toString.getBytes("UTF-8")
    )
    augmentString("%032x").format(new BigInteger(digest)).reverse.padTo(64, "0").reverse.mkString
  }

  @tailrec
  def waitForEsConnection(ctx: GlobalContext): Unit = {
    try {
      val esFuture = ctx.esClient.clusterInfo.map { clusterState =>
        scribe.info(s"Connected to Elasticsearch cluster: ${clusterState.clusterName}")
      }
      Await.result(esFuture, 10.seconds)
    } catch {
      case _: Throwable =>
        scribe.warn("Waiting for Elasticsearch availability...")
        Thread.sleep(10000)
        waitForEsConnection(ctx)
    }
  }

  def waitForKibanaConnection(ctx: GlobalContext): Unit = {
    try {
      val responseFuture = Http().singleRequest(
        HttpRequest(
          method  = HttpMethods.POST,
          uri     = ctx.config.kibana.server + "/api/index_patterns/default",
          headers = List(
            Authorization(BasicHttpCredentials(ctx.config.kibana.username, ctx.config.kibana.password)),
            headers.RawHeader("kbn-xsrf", "true")
          )
        )
      )

      val response = Await.result(responseFuture, 10.seconds)
      if (response.entity.contentType != ContentTypes.`application/json`) {
        scribe.warn("Waiting for Kibana availability...")
        Thread.sleep(10000)
        waitForKibanaConnection(ctx)
      }
    } catch {
      case _: Throwable =>
        scribe.warn("Waiting for Kibana availability...")
        Thread.sleep(10000)
        waitForKibanaConnection(ctx)
    }
  }

}
